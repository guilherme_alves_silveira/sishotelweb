package br.com.jegss.util;

import java.util.HashMap;
import java.util.Map;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
//import javax.persistence.spi.PersistenceUnitTransactionType;
//import static org.eclipse.persistence.config.PersistenceUnitProperties.TRANSACTION_TYPE;

@ApplicationScoped
public class EntityManagerProducer
{
    private static final String PERSISTENCE_UNIT = "SisHotelPU";
    private static final EntityManagerFactory FACTORY;
    private static final Map<String, Object> PROPERTIES;

    static
    {
        PROPERTIES = new HashMap<>();
        /*
        PROPERTIES.put(TRANSACTION_TYPE, PersistenceUnitTransactionType.JTA.name());
        PROPERTIES.put("javax.persistence.jdbc.url", "jdbc:mysql://localhost:3306/sis_hotel");
        PROPERTIES.put("javax.persistence.jdbc.user", "root");
        PROPERTIES.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");
        PROPERTIES.put("javax.persistence.jdbc.password", "");
        FACTORY = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT, PROPERTIES);*/
        FACTORY = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
    }

    private EntityManagerProducer()
    {
        //Construtor privado pois está classe segue o padrão Singleton
    }

    /**
     * @return uma EntityManager para que seja realizada a conexão com o banco
     * de dados
     */
    @Produces
    @RequestScoped
    public EntityManager getEntityManager()
    {
        return FACTORY.createEntityManager();
    }
    
    /**
     * Fecha o EntityManager.
     *
     * @param em
     */
    public void closeEntityManager(@Disposes EntityManager em)
    {
        em.close();
    }

    /**
     * Fecha a fabrica de EntityManager. Obs.: A conexão com o banco de dados é
     * fechada.
     */
    public static void closeEntityManagerFactory()
    {
        try
        {
            FACTORY.close();
        }
        catch (Exception e)
        {
            System.err.println("ERRO AO FECHAR CONEXÃO: " + e.getMessage());
        }
    }
}