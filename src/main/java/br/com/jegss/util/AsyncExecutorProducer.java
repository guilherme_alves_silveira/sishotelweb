package br.com.jegss.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class AsyncExecutorProducer
{
    private ExecutorService executor;
    
    @PostConstruct
    public void init()
    {
        this.executor = Executors.newCachedThreadPool();
    }
    
    @Produces
    public ExecutorService getAsyncExecutor()
    {
        return executor;
    }
    
    @PreDestroy
    public void finish()
    {
        executor.shutdown();
    }
}
