package br.com.jegss.util;

import com.google.gson.Gson;
import java.util.List;

public class JsonUtil<T>
{
    private final Gson json = new Gson();

    public String getResults(List<T> resultados)
    {
        String serialized = "{\"Result\":\"OK\",\"Records\":" + json.toJson(resultados) + "}";
        return serialized;
    }
}
