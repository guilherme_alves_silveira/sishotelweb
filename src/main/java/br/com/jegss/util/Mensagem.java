package br.com.jegss.util;

public class Mensagem
{
    private final String result;
    private final String message;

    public Mensagem(String result, String message)
    {
        this.result = result;
        this.message = message;
    }
    
    public String getResult()
    {
        return result;
    }

    public String getMessage()
    {
        return message;
    }
}
