package br.com.jegss.util;

import java.math.BigDecimal;

public class SistemaUtils
{

    private static final Object lock = new Object();

    /**
     * Classe centralizada que determina o máximo de desconto permitido
     *
     * @param valor Valor a ser passado
     *
     * @return BigDecimal
     */
    public static BigDecimal valorPermitidoPeloSistemaDeAcordoComPorcentagem(BigDecimal valor)
    {
        synchronized (lock)
        {
            return valor;
        }
    }

    public static synchronized BigDecimal horasParaCalculoDaDiaria()
    {
        return new BigDecimal(12);
    }
}