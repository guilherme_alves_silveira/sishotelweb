package br.com.jegss.util;

import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.sessions.factories.SessionCustomizer;

public class EnableIntegrityChecker implements SessionCustomizer
{
    @Override
    public void customize(Session session) throws Exception
    {
        session.getIntegrityChecker().checkDatabase();
        session.getIntegrityChecker().setShouldCatchExceptions(false);
    }
}