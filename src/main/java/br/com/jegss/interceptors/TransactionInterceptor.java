package br.com.jegss.interceptors;

import br.com.caelum.vraptor.Accepts;
import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.controller.ControllerMethod;
import br.com.caelum.vraptor.interceptor.SimpleInterceptorStack;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

@Intercepts
public class TransactionInterceptor
{
    private final EntityManager em;
    private final ControllerMethod method;

    @Inject
    public TransactionInterceptor(ControllerMethod method, EntityManager em)
    {
        this.method = method;
        this.em = em;
    }

    @Deprecated
    public TransactionInterceptor()
    {
        this(null, null);
    }

    @Accepts
    public boolean accepts(ControllerMethod method)
    {
        return method.containsAnnotation(Transaction.class);
    }

    @AroundCall
    public void intercept(SimpleInterceptorStack stack)
    {
        EntityTransaction tran = em.getTransaction();
        
        try
        {
            tran.begin();
            stack.next();
            tran.commit();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            if (tran != null && tran.isActive())
            {
                tran.rollback();
            }
        }
    }
}
