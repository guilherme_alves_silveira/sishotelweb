package br.com.jegss.controller;

import br.com.caelum.vraptor.Controller;
import br.com.jegss.seguranca.UsuarioLogado;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.com.jegss.model.dao.UsuarioDAO;
import br.com.jegss.seguranca.Open;
import br.com.jegss.model.po.Usuario;
import javax.inject.Inject;

@Controller
public class LoginController
{
    private final UsuarioDAO usuarioDAO;
    private final UsuarioLogado usuarioLogado;
    private final Result result;
    private final Validator validator;

    @Inject
    public LoginController(UsuarioDAO usuarioDAO, UsuarioLogado usuarioLogado,
                           Result result, Validator validator)
    {
        this.usuarioDAO = usuarioDAO;
        this.usuarioLogado = usuarioLogado;
        this.result = result;
        this.validator = validator;
    }

    @Deprecated
    public LoginController()
    {
        this(null, null, null, null);
    }

    @Open
    @Get
    public void form()
    {
        // -> /WEB-INF/login/form
    }

    @Open
    @Post
    public void autentica(String login, String senha)
    {
        Usuario usuario = usuarioDAO.busca(login, senha);
        if (usuario != null)
        {
            usuarioLogado.fazLogin(usuario);
            result.redirectTo(HomeController.class).index();
        }
        else
        {
            validator.add(new SimpleMessage("login_invalido", "Usuário ou senha inválidos."));
            validator.onErrorForwardTo(this).form();
        }
    }

    @Open
    @Get
    public void desloga()
    {
        usuarioLogado.desloga();
        result.redirectTo(this).form();
    }
}