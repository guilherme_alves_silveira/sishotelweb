package br.com.jegss.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.com.jegss.controller.util.PessoaControllerUtil;
import br.com.jegss.interceptors.Transaction;
import br.com.jegss.model.dao.DependenteDAO;
import br.com.jegss.model.po.EnderecoDependente;
import br.com.jegss.model.po.Dependente;
import br.com.jegss.model.po.Hospede;
import br.com.jegss.util.JsonUtil;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.validation.Valid;

@Controller
public class DependenteController
{

    private final Validator validator;
    private final Result result;
    private final DependenteDAO dao;
    private final PessoaControllerUtil<Dependente, EnderecoDependente, DependenteDAO> controllerUtil;

    @Inject
    public DependenteController(DependenteDAO dao, Result result, Validator validator, JsonUtil<Dependente> json)
    {
        this.controllerUtil = new PessoaControllerUtil(dao, result, json);
        this.result = result;
        this.dao = dao;
        this.validator = validator;
    }

    @Deprecated
    public DependenteController()
    {
        this(null, null, null, null);
    }

    @Get("dependente/lista/{first}/{max}")
    public void lista(int first, int max)
    {
        controllerUtil.lista(first, max);
    }

    @Put
    @Transaction
    public void atualiza(String cpfResponsavel, String id,
                         @Valid Dependente dependente, @Valid EnderecoDependente endereco)
    {
        validaIdDependente(id, dependente);
        validator.onErrorRedirectTo(this).form();
        controllerUtil.atualiza(id, dependente, endereco);
        result.redirectTo(this).form();
    }

    @Post
    public void salva(String cpfResponsavel, @Valid Dependente dependente,
                      @Valid EnderecoDependente endereco)
    {
        validaCpfResponsavel(cpfResponsavel, dependente);
        validator.onErrorRedirectTo(this).form();
        controllerUtil.salva(dependente, endereco);
        result.redirectTo(this).form();
    }

    @Delete
    @Transaction
    public void deleta(String id, String cpfResponsavel, Dependente dependente, EnderecoDependente endereco)
    {
        validaIdDependente(id, dependente);
        setIdResponsaveis(dependente);
        validator.onErrorForwardTo(this).form();
        controllerUtil.deleta(id, dependente, endereco);
        result.redirectTo(this).form();
    }

    @Get
    public void quantidade()
    {
        controllerUtil.quantidade();
    }

    @Get("dependente/busca/{id}")
    public void buscaPorId(String id)
    {
        controllerUtil.buscaPorId(id);
    }

    @Get
    public void form()
    {
    }

    private void setIdResponsaveis(Dependente dependente)
    {
        try
        {
            dao.setResponsaveisNoDependente(dependente);
        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
    }

    private void validaCpfResponsavel(String cpfResponsavel, Dependente dependente)
    {
        boolean valido = true;
        try
        {
            Long idResponsavel = dao.getResponsavelPeloCPF(cpfResponsavel);
            Hospede responsavel = new Hospede();
            responsavel.setId(idResponsavel);
            dependente.adicionaResponsavel(responsavel);
        }
        catch (Exception ex)
        {
            valido = false;
        }

        validator.ensure(cpfResponsavel != null && valido,
                         new SimpleMessage("cpfResponsavel", "O responsável deverá estar cadastrado no sistema com este CPF, verifique se o mesmo se encontrada cadastrado."));
    }

    private void validaIdDependente(String id, Dependente dependente)
    {
        boolean valido = true;
        try
        {
            long nid = Long.parseUnsignedLong(id);
            dependente.setId(nid);
        }
        catch (Exception e)
        {
            valido = false;
        }

        validator.ensure(id != null && valido,
                         new SimpleMessage("id", "Informe o id por gentileza, para que seja necessário atualizar."));
    }
}
