package br.com.jegss.controller.util;

import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.jegss.model.dao.PessoaDAO;
import br.com.jegss.model.po.Endereco;
import br.com.jegss.model.po.Pessoa;
import br.com.jegss.util.JsonUtil;
import com.google.common.base.Strings;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;

public class PessoaControllerUtil<T extends Pessoa, E extends Endereco, DAO extends PessoaDAO>
{
    private final DAO dao;
    private final Result result;
    private final JsonUtil<T> json;

    public PessoaControllerUtil(DAO dao, Result result, JsonUtil<T> json)
    {
        this.dao = dao;
        this.result = result;
        this.json = json;
    }

    @Deprecated
    public PessoaControllerUtil()
    {
        this(null, null, null);
    }

    public void lista(int first, int max)
    {
        if (first < 0 || max > 500)
        {
            first = 0;
            max = 500;
        }

        List<T> pessoas = dao.buscaPaginadaDeDados(first, max);
        String serialized = json.getResults(pessoas);
        result.use(Results.http())
                .body(serialized)
                .addHeader("content-type", "application/json");
    }

    public void atualiza(String id, T pessoa, E endereco)
    {
        Long idEndereco = dao.getEnderecoID(pessoa);
        endereco.setId(idEndereco);
        salva(pessoa, endereco);
    }

    public void salva(T pessoa, E endereco)
    {
        EntityManager em = dao.getEntityManager();
        em.getTransaction().begin();
        pessoa = (T) dao.getSalvaOuAtualiza(pessoa);
        em.getTransaction().commit();
        //Salva/Atuliza o endeço
        em.getTransaction().begin();
        endereco.setPessoa(pessoa);
        em.merge(endereco);
        em.getTransaction().commit();
    }

    public void deleta(String id, T pessoa, E endereco)
    {
        Long idEndereco = dao.getEnderecoID(pessoa);
        endereco.setId(idEndereco);
        endereco.setPessoa(pessoa);
        dao.deleta(pessoa);
    }

    public void quantidade()
    {
        long quantidade = dao.buscaTotalDeItens();
        result.use(Results.json())
              .withoutRoot()
              .from(quantidade)
              .serialize();
    }

    public void buscaPorId(String id)
    {
        try
        {
            if (Strings.isNullOrEmpty(id))
            {
                throw new NumberFormatException();
            }
            Long nid = Long.valueOf(id);
            T pessoa = (T) dao.pegaPorId(nid);
            if (pessoa == null) throw new EntityNotFoundException();
            result.use(Results.json())
                    .withoutRoot()
                    .from(pessoa)
                    .include("endereco")
                    .serialize();
        }
        catch (NumberFormatException | EntityNotFoundException e)
        {
            result.use(Results.json())
                    .from("O hospede informado não existe.", "error")
                    .serialize();
        }
    }
}