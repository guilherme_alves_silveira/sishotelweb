package br.com.jegss.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.com.jegss.controller.util.PessoaControllerUtil;
import br.com.jegss.interceptors.Transaction;
import br.com.jegss.model.dao.FuncionarioDAO;
import br.com.jegss.model.po.EnderecoFuncionario;
import br.com.jegss.model.po.Funcionario;
import br.com.jegss.util.JsonUtil;
import javax.inject.Inject;
import javax.validation.Valid;

@Controller
public class FuncionarioController
{
    private final Validator validator;
    private final Result result;
    private final PessoaControllerUtil<Funcionario, EnderecoFuncionario, FuncionarioDAO> controllerUtil;

    @Inject
    public FuncionarioController(FuncionarioDAO dao, Result result, Validator validator, JsonUtil<Funcionario> json)
    {
        this.controllerUtil = new PessoaControllerUtil(dao, result, json);
        this.result = result;
        this.validator = validator;
    }

    @Deprecated
    public FuncionarioController()
    {
        this(null, null, null, null);
    }

    @Get("funcionario/lista/{first}/{max}")
    public void lista(int first, int max)
    {
        controllerUtil.lista(first, max);
    }

    @Put
    @Transaction
    public void atualiza(String id, @Valid Funcionario funcionario, @Valid EnderecoFuncionario endereco)
    {
        validaIdFuncionario(id, funcionario);
        validator.onErrorRedirectTo(this).form();
        controllerUtil.atualiza(id, funcionario, endereco);
        result.redirectTo(this).form();
    }

    @Post
    public void salva(@Valid Funcionario funcionario, @Valid EnderecoFuncionario endereco)
    {
        validator.onErrorRedirectTo(this).form();
        controllerUtil.salva(funcionario, endereco);
        result.redirectTo(this).form();
    }

    @Delete
    @Transaction
    public void deleta(String id, Funcionario funcionario, EnderecoFuncionario endereco)
    {
        validaIdFuncionario(id, funcionario);
        validator.onErrorForwardTo(this).form();
        controllerUtil.deleta(id, funcionario, endereco);
        result.redirectTo(this).form();
    }

    @Get
    public void quantidade()
    {
        controllerUtil.quantidade();
    }

    @Get("funcionario/busca/{id}")
    public void buscaPorId(String id)
    {
        controllerUtil.buscaPorId(id);
    }

    @Get
    public void form()
    {
    }

    private void validaIdFuncionario(String id, Funcionario funcionario)
    {
        boolean valido = true;
        try
        {
            long nid = Long.parseUnsignedLong(id);
            funcionario.setId(nid);
        }
        catch (NumberFormatException e)
        {
            valido = false;
        }
        validator.ensure(id != null && valido, new SimpleMessage("id", "Informe o id por gentileza, para que seja necessário atualizar."));
    }
}