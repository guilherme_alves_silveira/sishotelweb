package br.com.jegss.controller;

import br.com.jegss.controller.util.PessoaControllerUtil;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.http.VRaptorRequest;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.com.caelum.vraptor.view.Results;
import br.com.caelum.vraptor.view.TemplateAsyncReadListener;
import br.com.jegss.interceptors.Transaction;
import br.com.jegss.model.dao.HospedeDAO;
import br.com.jegss.model.po.EnderecoHospede;
import br.com.jegss.model.po.Hospede;
import br.com.jegss.util.JsonUtil;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;

@Controller
public class HospedeController
{

    private final Validator validator;
    private final Result result;
    private final PessoaControllerUtil<Hospede, EnderecoHospede, HospedeDAO> controllerUtil;
    private final HospedeDAO dao;
    private final JsonUtil<Hospede> json;

    @Inject
    public HospedeController(HospedeDAO dao, Result result, Validator validator, JsonUtil<Hospede> json)
    {
        this.controllerUtil = new PessoaControllerUtil(dao, result, json);
        this.result = result;
        this.validator = validator;
        this.dao = dao;
        this.json = json;
    }

    @Deprecated
    public HospedeController()
    {
        this(null, null, null, null);
    }

    @Get("hospede/async")
    public void async(VRaptorRequest req)
    {
        try
        {
            req.beginAsyncRead(new TemplateAsyncReadListener()
            {
                @Override
                public void executeLogic()
                {
                    final List<Hospede> hospedes = dao.buscaPaginadaDeDados(0, 2000);
                    String serialized = json.getResults(hospedes);
                    result.use(Results.http()).body(serialized, getAsync()).addHeader("content-type", "application/json");
                }
            });
        }
        catch (IOException ex)
        {
            System.out.println(ex);
        }
    }

    @Get("hospede/sync")
    public void sync()
    {
        final List<Hospede> hospedes = dao.buscaPaginadaDeDados(0, 2000);
        result.use(Results.json()).withoutRoot().from(hospedes).serialize();
    }

    @Get("hospede/lista/{first}/{max}")
    public void lista(int first, int max)
    {
        controllerUtil.lista(first, max);
    }

    @Put
    public void atualiza(String id, @Valid Hospede hospede, @Valid EnderecoHospede endereco)
    {
        validaIdHospede(id, hospede);
        validator.onErrorRedirectTo(this).form();
        controllerUtil.atualiza(id, hospede, endereco);
        result.redirectTo(this).form();
    }

    @Post
    public void salva(@Valid Hospede hospede, @Valid EnderecoHospede endereco)
    {
        validator.onErrorRedirectTo(this).form();
        controllerUtil.salva(hospede, endereco);
        result.redirectTo(this).form();
    }

    @Delete
    @Transaction
    public void deleta(String id, Hospede hospede, EnderecoHospede endereco)
    {
        validaIdHospede(id, hospede);
        validator.onErrorForwardTo(this).form();
        controllerUtil.deleta(id, hospede, endereco);
        result.redirectTo(this).form();
    }

    @Get("hospede/count")
    public void quantidade()
    {
        controllerUtil.quantidade();
    }

    @Get("hospede/busca/{id}")
    public void buscaPorId(String id)
    {
        controllerUtil.buscaPorId(id);
    }

    @Get
    public void form()
    {
    }

    private void validaIdHospede(String id, Hospede hospede)
    {
        boolean valido = true;
        try
        {
            Long nid = Long.valueOf(id);
            hospede.setId(nid);
        }
        catch (NumberFormatException e)
        {
            valido = false;
        }
        validator.ensure(id != null && valido, new SimpleMessage("id", "Informe o id por gentileza, para que seja necessário atualizar."));
    }
}
