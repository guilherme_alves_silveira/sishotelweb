package br.com.jegss.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Path;

@Controller
public class HomeController
{
    @Path(value = "/", priority = Path.HIGHEST)
    public void index()
    {
        //-> WEB-INF/jsp/home/index.jsp
    }
}