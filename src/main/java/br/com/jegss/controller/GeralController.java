package br.com.jegss.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.jegss.model.dao.GeralUtilDAO;
import br.com.jegss.model.po.Cidade;
import br.com.jegss.model.po.Estado;
import br.com.jegss.model.po.Funcao;
import java.util.List;
import javax.inject.Inject;

@Controller
public class GeralController
{
    private final Result result;
    private final GeralUtilDAO geralDAO;
    
    @Inject
    public GeralController(Result result, GeralUtilDAO geralDAO)
    {
        this.result = result;
        this.geralDAO = geralDAO;
    }
    
    @Deprecated
    public GeralController()
    {
        this(null, null);
    }
    
    @Get("geral/cidade/{id}")
    public void cidadePorEstado(String id)
    {
        Estado estado = geralDAO.buscaEstado(Short.valueOf(id));
        List<Cidade> cidades = geralDAO.listaCidadesDeAcordoComOEstadoSelecionado(estado.getUf());
        result.use(Results.json())
              .withoutRoot()
              .from(cidades)
              .serialize();
    }
    
    @Get("geral/estados")
    public void estados()
    {
        List<Estado> estados = geralDAO.listaEstados();
        result.use(Results.json())
              .withoutRoot()
              .from(estados)
              .serialize();
    }
    
    @Get("geral/funcoes")
    public void funcoes()
    {
        List<Funcao> funcoes = geralDAO.listaFuncoesDeFuncionario();
        result.use(Results.json())
              .withoutRoot()
              .from(funcoes)
              .serialize();
    }
}