package br.com.jegss.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.com.caelum.vraptor.view.Results;
import br.com.jegss.interceptors.Transaction;
import br.com.jegss.model.dao.QuartoDAO;
import br.com.jegss.model.po.Quarto;
import br.com.jegss.util.JsonUtil;
import com.google.common.base.Strings;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@Controller
public class QuartoController
{
    private final JsonUtil<Quarto> json;
    private final QuartoDAO dao;
    private final Validator validator;
    private final Result result;

    @Inject
    public QuartoController(JsonUtil<Quarto> json, QuartoDAO dao, Validator validator, Result result)
    {
        this.json = json;
        this.dao = dao;
        this.validator = validator;
        this.result = result;
    }

    @Deprecated
    public QuartoController()
    {
        this(null, null, null, null);
    }

    @Get("quarto/lista/{first}/{max}")
    public void lista(int first, int max)
    {
        List<Quarto> quartos = dao.buscaPaginadaDeDados(first, max);
        String serialized = json.getResults(quartos);
        result.use(Results.http())
                .body(serialized)
                .addHeader("content-type", "application/json");
    }

    @Get("quarto/busca/{id}")
    public void buscaPorId(String id)
    {
        try
        {
            if (Strings.isNullOrEmpty(id))
            {
                throw new NumberFormatException();
            }
            Short nid = Short.valueOf(id);
            Quarto quarto = dao.pegaPorId(nid, false);
            if (quarto == null) throw new EntityNotFoundException();
            result.use(Results.json())
                    .withoutRoot()
                    .from(quarto)
                    .serialize();
        }
        catch (NumberFormatException | EntityNotFoundException e)
        {
            result.use(Results.json())
                  .from("O quarto informado não existe.", "error")
                  .serialize();
        }
    }

    @Get
    public void form()
    {
    }

    @Post
    @Transaction
    public void salva(@Valid Quarto quarto)
    {
        validator.onErrorRedirectTo(this).form();
        dao.salvaOuAtualiza(quarto);
        result.redirectTo(this).form();
    }

    @Delete
    @Transaction
    public void deleta(String id, Quarto quarto)
    {
        validaIdQuarto(id, quarto);
        validator.onErrorRedirectTo(this).form();
        dao.deleta(quarto, false);
        result.redirectTo(this).form();
    }

    @Put
    public void atualiza(String id, Quarto quarto)
    {
        validaIdQuarto(id, quarto);
        salva(quarto);
    }

    private void validaIdQuarto(String id, Quarto quarto)
    {
        boolean valido = true;
        try
        {
            Short nid = Short.valueOf(id);
            quarto.setId(nid);
        }
        catch (NumberFormatException e)
        {
            valido = false;
        }
        
        validator.ensure(id != null && valido, new SimpleMessage("id", "Informe o id por gentileza, para que seja necessário atualizar."));
    }
}
