package br.com.jegss.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.com.caelum.vraptor.view.Results;
import br.com.jegss.interceptors.Transaction;
import br.com.jegss.model.dao.ReservaDAO;
import br.com.jegss.model.po.Reserva;
import br.com.jegss.util.JsonUtil;
import com.google.common.base.Strings;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;

@Controller
public class ReservaController
{

    private final JsonUtil<Reserva> json;
    private final ReservaDAO dao;
    private final Validator validator;
    private final Result result;

    @Inject
    public ReservaController(JsonUtil<Reserva> json, ReservaDAO dao, Validator validator, Result result)
    {
        this.json = json;
        this.dao = dao;
        this.validator = validator;
        this.result = result;
    }

    @Deprecated
    public ReservaController()
    {
        this(null, null, null, null);
    }

    @Get("reserva/lista/{first}/{max}")
    public void lista(int first, int max)
    {
        List<Reserva> reservas = dao.buscaPaginadaDeDados(first, max);
        String serialized = json.getResults(reservas);
        String dataASubstituir = "('year':)|('month':)|('dayOfMonth':)|('hourOfDay':(,0|0,))|('minute':(,0|0,))".replace("'", "\"");
        serialized = serialized.replaceAll(dataASubstituir, "").replace(",\"second\":0", "");
        result.use(Results.http())
                .body(serialized)
                .addHeader("content-type", "application/json");
    }

    @Get("reserva/busca/{id}")
    public void buscaPorId(String id)
    {
        try
        {
            if (Strings.isNullOrEmpty(id))
            {
                throw new NumberFormatException();
            }
            Long nid = Long.valueOf(id);
            Reserva reserva = dao.pegaPorId(nid, false);
            if (reserva == null)
            {
                throw new EntityNotFoundException();
            }
            result.use(Results.json())
                    .withoutRoot()
                    .from(reserva)
                    .serialize();
        }
        catch (NumberFormatException | EntityNotFoundException e)
        {
            result.use(Results.json())
                    .from("O reserva informado não existe.", "error")
                    .serialize();
        }
    }

    @Get
    public void form()
    {
    }

    @Post
    @Transaction
    public void salva(Long idHospede, Reserva reserva)
    {
        reserva.setHospede(idHospede);
        validator.validate(reserva).onErrorRedirectTo(this).form();
        dao.salvaOuAtualiza(reserva);
        result.redirectTo(this).form();
    }

    @Delete
    @Transaction
    public void deleta(String id, Reserva reserva)
    {
        validaIdReserva(id, reserva).onErrorRedirectTo(this).form();
        dao.deleta(reserva, false);
        result.redirectTo(this).form();
    }

    @Put
    public void atualiza(Long idHospede, String id, Reserva reserva)
    {
        validaIdReserva(id, reserva);
        salva(idHospede, reserva);
    }

    private Validator validaIdReserva(String id, Reserva reserva)
    {
        boolean valido = true;
        try
        {
            Long nid = Long.valueOf(id);
            reserva.setId(nid);
        }
        catch (NumberFormatException e)
        {
            valido = false;
        }

        validator.ensure(id != null && valido, new SimpleMessage("id", "Informe o id por gentileza, para que seja necessário atualizar."));
        return validator;
    }
}
