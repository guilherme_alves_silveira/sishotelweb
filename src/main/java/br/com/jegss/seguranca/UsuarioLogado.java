package br.com.jegss.seguranca;

import br.com.jegss.model.po.Usuario;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class UsuarioLogado implements Serializable
{
    private Usuario usuario;
    
    public void fazLogin(Usuario usuario)
    {
        this.usuario = usuario;
    }
    
    public void desloga()
    {
        usuario = null;
    }
    
    public boolean isLogado()
    {
        return usuario != null;
    }
    
    public Usuario getUsuario()
    {
        return usuario;
    }
}