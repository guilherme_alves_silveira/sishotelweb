package br.com.jegss.model.vo;

import com.google.common.base.Strings;
import java.math.BigDecimal;

public class Dinheiro
{

    private String montante;
    private String dinheiro;
    private BigDecimal valor;

    public Dinheiro(String montante, String dinheiro)
    {
        converteEmBigDecimal(montante, dinheiro);
    }

    public void converteEmBigDecimal(String montante, String dinheiro)
    {
        try
        {
            boolean montanteValido = !Strings.isNullOrEmpty(montante) && montante.trim().equals("R$");
            boolean dinheiroValido = !Strings.isNullOrEmpty(dinheiro);
            if (montanteValido && dinheiroValido)
            {
                boolean contemVirgulaEPonto = dinheiro.contains(".") && dinheiro.contains(",");
                if (contemVirgulaEPonto)
                {
                    dinheiro = dinheiro.replace(".", "");
                }
                dinheiro = dinheiro.replace(",", ".").trim();
                this.montante = montante;
                this.dinheiro = dinheiro;
                this.valor = new BigDecimal(dinheiro);
            }
            else
            {
                throw new NumberFormatException("O montante aceito é somente em Reais e o valor do dinheiro informado é inválido.");
            }
        }
        catch (NumberFormatException e)
        {
            throw new NumberFormatException("O número foi passado em um formato inválido. Formato: " + dinheiro);
        }
    }

    public BigDecimal getValor()
    {
        return valor;
    }

    public String getMontante()
    {
        return montante;
    }

    public String getDinheiro()
    {
        return dinheiro;
    }
}
