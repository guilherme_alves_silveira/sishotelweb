package br.com.jegss.model.po.converter;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.converter.Converter;
import br.com.jegss.model.po.Cidade;
import javax.annotation.Priority;
import javax.interceptor.Interceptor;

@Convert(Cidade.class)
@Priority(Interceptor.Priority.APPLICATION)
public class CidadeConverter implements Converter<Cidade>
{

    @Override
    public Cidade convert(String value, Class<? extends Cidade> type)
    {
        Cidade cidade = new Cidade();
        try
        {
            int id = Integer.parseInt(value);
            cidade.setId(id);
        }
        catch (NumberFormatException ex)
        {
            //Ignora pois deverá ser pego pelo Validator.
        }
        
        return cidade;
    }
}