package br.com.jegss.model.po;

import br.com.jegss.model.po.enumerated.EstadoDiaria;
import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "diaria")
public class Diaria extends Entidade<Long>
{
    @Column(name = "data_entrada")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar dataEntrada;

    @Column(name = "data_saida")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar dataSaida;
    
    @Column(name = "valor_diaria", 
            nullable = false,
            precision = 10,
            scale = 8)
    private BigDecimal valorDiaria;
    
    @Column(name = "vencimento")
    private int vencimento;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "estado")
    private EstadoDiaria estado;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_quarto")
    private Quarto quarto;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_reserva")
    private Reserva reserva;

    @ManyToOne(optional = false)
    @JoinColumn(name = "id_hospede")
    private Hospede hospede;

    public Diaria()
    {
        this.estado = EstadoDiaria.ABERTO;
    }

    public Diaria(Reserva reserva)
    {
        this();
        this.setReserva(reserva);
    }

    @NotNull(message = "Insira a data de entrada.")
    public Calendar getDataEntrada()
    {
        return dataEntrada;
    }

    public void setDataEntrada(Calendar dataEntrada)
    {
        this.dataEntrada = dataEntrada;
    }

    public Calendar getDataSaida()
    {
        return dataSaida;
    }

    public void setDataSaida(Calendar dataSaida)
    {
        this.dataSaida = dataSaida;
    }

    public int getVencimento()
    {
        return vencimento;
    }

    public void setVencimento(int vencimento)
    {
        this.vencimento = vencimento;
    }

    public String getEstado()
    {
        return estado.getDescricao();
    }

    public Quarto getQuarto()
    {
        return quarto;
    }

    public void setQuarto(Quarto quarto)
    {
        if(quarto == null)
        {
            throw new NullPointerException("O Quarto não pode ser nulo.");
        }
        this.quarto = quarto;
    }

    public Reserva getReserva()
    {
        return reserva;
    }

    public void setReserva(Reserva reserva)
    {
        if(reserva == null)
        {
            throw new NullPointerException("A Reserva não pode ser nula.");
        }
        this.reserva = reserva;
        this.setQuarto(reserva.getQuarto());
        this.hospede = reserva.getHospede();
    }
    
    public Hospede getHospede()
    {
        return hospede;
    }

    public void setHospede(Hospede hospede)
    {
        this.hospede = hospede;
    }
    
    public BigDecimal getValor()
    {
        return valorDiaria;
    }

    public void setValor(BigDecimal valorDiaria)
    {
        if(valorDiaria.doubleValue() > 0)
        {
            this.valorDiaria = valorDiaria;
        }
        else
        {
            throw new IllegalArgumentException("O valor da diária deve ser maior que zero.");
        }
    }
    
    public void abreDiaria()
    {
        if(estado != EstadoDiaria.FECHADO)
        {
            this.estado = EstadoDiaria.ABERTO;
        }
        else
        {
            throw new IllegalArgumentException("Diária já finalizada.");
        }
    }
    
    public void fechaDiaria()
    {
        if(estado != EstadoDiaria.FECHADO)
        {
            this.estado = EstadoDiaria.FECHADO;
        }
        else
        {
            throw new IllegalArgumentException("Diária já finalizada.");
        }
    }
}