package br.com.jegss.model.po;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "endereco_hospede")
public class EnderecoHospede extends Endereco<Hospede>
{
    @OneToOne(fetch = FetchType.EAGER, 
              cascade = CascadeType.ALL)
    @JoinColumn(name = "id_hospede",
                referencedColumnName = "id",
                nullable = false)
    private Hospede hospede;

    public Hospede getHospede()
    {
        return hospede;
    }

    public void setHospede(Hospede hospede)
    {
        this.hospede = hospede;
        hospede.setEndereco(this);
    }

    @Override
    public void setPessoa(Hospede hospede)
    {
        setHospede(hospede);
    }
}