package br.com.jegss.model.po;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

@Entity
@Table(name = "pais")
@Cacheable(true)
@Cache(size = 200,
       type = CacheType.SOFT)
@NamedQueries
({
    @NamedQuery(name = "Pais.findAllPaises", query = "SELECT p FROM Pais p")
})
public class Pais extends Entidade<Short>
{
    @Column(name = "nome", 
            length = 50,
            nullable = false)
    private String nome;
    
    @Column(name = "codigo",
            length = 10,
            nullable = false)
    private String codigo;

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    @Override
    public String toString()
    {
        return "Pais{" + "nome=" + nome + ", codigo=" + codigo + '}';
    }
}