package br.com.jegss.model.po.converter;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.converter.Converter;
import br.com.jegss.model.po.enumerated.Sexo;

@Convert(Sexo.class)
public class SexoConverter implements Converter<Sexo>
{
    @Override
    public Sexo convert(String value, Class<? extends Sexo> type)
    {
        switch(value)
        {
            case "M":
                return Sexo.M;
            case "F":
                return Sexo.F;
            case "O":
                return Sexo.O;
            default:
                return null;
        }
    }
}