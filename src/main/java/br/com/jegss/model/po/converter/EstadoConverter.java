package br.com.jegss.model.po.converter;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.converter.Converter;
import br.com.jegss.model.po.Estado;
import javax.annotation.Priority;
import javax.interceptor.Interceptor;

@Convert(Estado.class)
@Priority(Interceptor.Priority.APPLICATION)
public class EstadoConverter implements Converter<Estado>
{

    @Override
    public Estado convert(String value, Class<? extends Estado> type)
    {
        Estado estado = new Estado();
        try
        {
            short id = Short.parseShort(value);
            estado.setId(id);
        }
        catch (NumberFormatException ex)
        {
            //Ignora pois deverá ser pego pelo Validator.
        }
        
        return estado;
    }
}
