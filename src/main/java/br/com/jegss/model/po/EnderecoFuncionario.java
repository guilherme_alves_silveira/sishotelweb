package br.com.jegss.model.po;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "endereco_funcionario")
public class EnderecoFuncionario extends Endereco<Funcionario>
{
    @OneToOne(fetch = FetchType.EAGER,
              cascade = CascadeType.ALL)
    @JoinColumn(name = "id_funcionario",
                referencedColumnName = "id",
                nullable = false)
    private Funcionario funcionario;

    public Funcionario getFuncionario()
    {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario)
    {
        this.funcionario = funcionario;
        this.funcionario.setEndereco(this);
    }

    @Override
    public void setPessoa(Funcionario funcionario)
    {
        setFuncionario(funcionario);
    }
}