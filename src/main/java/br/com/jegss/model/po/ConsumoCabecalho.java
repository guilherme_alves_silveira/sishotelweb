package br.com.jegss.model.po;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.swing.JOptionPane;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "consumo_cabecalho")
@NamedQueries(
        {
            @NamedQuery(name = "ConsumoCabecalho.findConsumoCabecalhoComUnitariosById",
                        query = "SELECT c FROM ConsumoCabecalho c "
                        + "LEFT JOIN FETCH c.consumosUnitarios u WHERE c.id = :id"),
            @NamedQuery(name = "ConsumoCabecalho.findAll",
                        query = "SELECT c FROM ConsumoCabecalho c"),
            @NamedQuery(name = "ConsumoCabecalho.findLastNumber",
                        query = "SELECT MAX(c.id) + 1 FROM ConsumoCabecalho c"),
            @NamedQuery(name = "ConsumoCabecalho.findAllConstructor", 
                        query = "SELECT NEW br.com.jegss.model.po.ConsumoCabecalho(c.id, c.totalGeral, c.totalGeralComDesconto) FROM ConsumoCabecalho c")
        })
public class ConsumoCabecalho extends Entidade<Long>
{
    private transient CalculaConsumo calculadora;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_hospede",
                referencedColumnName = "id",
                nullable = false)
    private Hospede hospede;

    @OneToMany(mappedBy = "cabecalho")
    private List<ConsumoUnitario> consumosUnitarios;

    @Column(name = "total_geral",
            precision = 10,
            scale = 8,
            nullable = false)
    private BigDecimal totalGeral;

    @Column(name = "total_geral_com_desconto",
            precision = 10,
            scale = 8,
            nullable = false)
    private BigDecimal totalGeralComDesconto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_pagamento",
                referencedColumnName = "id",
                nullable = false)
    private TipoPagamento tipoPagamento;

    public ConsumoCabecalho()
    {
        this.totalGeral = BigDecimal.ZERO;
        this.totalGeralComDesconto = BigDecimal.ZERO;
        this.calculadora = new CalculaConsumo();
        this.consumosUnitarios = new ArrayList<>();
    }
    
    public ConsumoCabecalho(Long id, BigDecimal totalGeral, BigDecimal totalGeralComDesconto)
    {
        this();
        this.setId(id);
        this.totalGeral = totalGeral;
        this.totalGeralComDesconto = totalGeralComDesconto;
    }

    public ConsumoCabecalho(Hospede hospede)
    {
        this();
        this.hospede = hospede;
    }

    public Hospede getHospede()
    {
        return hospede;
    }

    public void setHospede(Hospede hospede)
    {
        if (hospede != null)
        {
            this.hospede = hospede;
        }
        else
        {
            throw new NullPointerException("O hospede não pode ser nulo.");
        }
    }

    public List<ConsumoUnitario> getConsumosUnitarios()
    {
        /**
         * Retorna uma lista não modificável,
         */
        return Collections.unmodifiableList(consumosUnitarios);
    }

    public void adicionaConsumos(ConsumoUnitario consumo)
    {
        if (consumo != null)
        {
            boolean isNulo = Objects.isNull(consumo.getBordero()) == true ? true : consumo.getBordero().getId() <= 0;
            if (isNulo)
            {
                throw new IllegalArgumentException("O bordero do consumo unitário não pode ser nulo e deve ter id.");
            }
            /**
             * Se foi adicionado com sucesso na lista, realiza o calculo.
             */
            if (this.consumosUnitarios.contains(consumo))
            {
                /**
                 * Pega a posição do objeto a ser substituído na lista passando
                 * o objeto recebido no parâmetro, pois o que compara se um
                 * objeto ConsumoUnitario é igual ao outro, é o seu código. Após
                 * pegar a posição, o objeto é pego da lista, e após isso é
                 * removido da lista no método removeConsumo, que realiza o
                 * calculo de subtração do ConsumoUnitario removido.
                 */
                Integer posicao = consumosUnitarios.lastIndexOf(consumo);
                ConsumoUnitario consumoASerSubstituido = getConsumoAt(posicao);
                removeConsumo(consumoASerSubstituido);
                /**
                 * O calculo é refeito com o novo objeto.
                 */
                if (this.consumosUnitarios.add(consumo))
                {
                    calculadora.somaNoTotal(consumo);
                }
            }
            else if (this.consumosUnitarios.add(consumo))
            {
                calculadora.somaNoTotal(consumo);
            }
        }
        else
        {
            throw new NullPointerException("O consumo não pode estar vazio nem nulo.");
        }
    }

    public ConsumoUnitario getConsumoAt(int posicao)
    {
        try
        {
            ConsumoUnitario consumo = this.consumosUnitarios.get(posicao);
            return consumo;
        }
        catch (IndexOutOfBoundsException e)
        {
            JOptionPane.showMessageDialog(null, "Não existe este dependente nesta posição.");
        }
        return null;
    }

    public void removeConsumo(ConsumoUnitario consumoASerRemovido) throws IllegalArgumentException
    {
        if (consumoASerRemovido != null)
        {
            if (this.consumosUnitarios.remove(consumoASerRemovido))
            {
                calculadora.subtraiDoTotal(consumoASerRemovido);
            }
        }
        else
        {
            throw new NullPointerException("Consumo não pode ser nulo.");
        }
    }

    public BigDecimal getTotalGeral()
    {
        return totalGeral;
    }

    @NotNull(message = "tipo de pagamento")
    public TipoPagamento getTipoPagamento()
    {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento)
    {
        this.tipoPagamento = tipoPagamento;
    }

    public BigDecimal getTotalGeralComDesconto()
    {
        return totalGeralComDesconto;
    }

    /**
     * Classe interna que é utilizada para os calculos da classe
     * ConsumoCabecalho
     */
    private class CalculaConsumo
    {

        private void somaNoTotal(ConsumoUnitario consumo)
        {
            totalGeral = totalGeral.add(consumo.getTotal());
            totalGeralComDesconto = totalGeralComDesconto.add(consumo.getTotalComDesconto());
        }

        private void subtraiDoTotal(ConsumoUnitario consumo)
        {
            totalGeral = totalGeral.subtract(consumo.getTotal());
            totalGeralComDesconto = totalGeralComDesconto.subtract(consumo.getTotalComDesconto());
        }
    }
}