package br.com.jegss.model.po.converter;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.converter.Converter;
import br.com.jegss.model.vo.Dinheiro;

@Convert(Dinheiro.class)
public class DinheiroConverter implements Converter<Dinheiro>
{

    @Override
    public Dinheiro convert(String value, Class<? extends Dinheiro> type)
    {
        try
        {
            String[] valores = value.split(" ");
            String montante = valores[0];
            String dinheiro = valores[1];
            return new Dinheiro(montante, dinheiro);
        }
        catch (Exception e)
        {
            return null;
        }
    }
}
