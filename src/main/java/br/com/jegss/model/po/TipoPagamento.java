package br.com.jegss.model.po;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "tipo_pagamento")
public class TipoPagamento extends Entidade<Short>
{
    @Column(name = "descricao",
            nullable = false)
    private String descricao;

    public TipoPagamento()
    {
        //Construtor default
    }
    
    public TipoPagamento(String descricao)
    {
        this.descricao = descricao;
    }

    @NotBlank(message = "Preencha a descrição do Tipo de Pagamento.")
    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }
}