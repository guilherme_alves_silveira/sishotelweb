package br.com.jegss.model.po;

import br.com.jegss.model.po.enumerated.TipoConsumo;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@DiscriminatorValue(value = "SERVICO")
public class Servico extends Bordero
{
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "tempo")
    private Calendar tempo;
    
    public Servico()
    {
        //Construtor default
    }
    
    public Servico(Long id, TipoConsumo tipoConsumo)
    {
        super(id, tipoConsumo);
    }
    
    public Servico(String id, String descricao, TipoConsumo tipoConsumo)
    {
        super(id, descricao, tipoConsumo);
    }
    
    @NotNull(message = "Informe o tempo de serviço por favor.")
    public Calendar getTempo()
    {
        return tempo;
    }

    public void setTempo(Calendar tempo)
    {
        this.tempo = tempo;
    }
}