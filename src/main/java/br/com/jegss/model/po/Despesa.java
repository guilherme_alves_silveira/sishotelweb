package br.com.jegss.model.po;

import br.com.jegss.model.po.enumerated.TipoConsumo;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "DESPESA")
public class Despesa extends Bordero
{

    @Column(name = "prejuizo_causado_pelo_hospede")
    private boolean prejuizoCausadoPeloHospede;

    public Despesa()
    {
        //Construtor default
    }

    public Despesa(Long id, TipoConsumo tipoConsumo)
    {
        super(id, tipoConsumo);
    }

    public Despesa(String id, String descricao, TipoConsumo tipoConsumo)
    {
        super(id, descricao, tipoConsumo);
    }

    public Despesa(boolean prejuizoCausadoPeloHospede)
    {
        this.prejuizoCausadoPeloHospede = prejuizoCausadoPeloHospede;
    }

    public boolean isPrejuizoCausadoPeloHospede()
    {
        return prejuizoCausadoPeloHospede;
    }

    public void setPrejuizoCausadoPeloHospede(boolean prejuizoCausadoPeloHospede)
    {
        this.prejuizoCausadoPeloHospede = prejuizoCausadoPeloHospede;
    }
}