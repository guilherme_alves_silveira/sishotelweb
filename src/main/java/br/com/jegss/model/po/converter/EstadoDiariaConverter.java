package br.com.jegss.model.po.converter;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.converter.Converter;
import br.com.jegss.model.po.enumerated.EstadoDiaria;

@Convert(EstadoDiaria.class)
public class EstadoDiariaConverter implements Converter<EstadoDiaria>
{
    @Override
    public EstadoDiaria convert(String value, Class<? extends EstadoDiaria> type)
    {
        if (!"".equals(value))
        {
            value = value.trim();
        }
        
        switch (value)
        {
            case "Aberto":
            case "ABERTO": 
                return EstadoDiaria.ABERTO;
            case "Fechado":
            case "FECHADO":
                return EstadoDiaria.FECHADO;
            default:
                return EstadoDiaria.ABERTO;
        }
    }
}
