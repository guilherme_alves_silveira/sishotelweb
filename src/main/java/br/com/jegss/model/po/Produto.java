package br.com.jegss.model.po;

import br.com.jegss.model.po.enumerated.TipoConsumo;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "PRODUTO")
public class Produto extends Bordero
{
    @Column(name = "estoque", 
            precision = 10,
            scale = 8)
    private BigDecimal estoque;

    public Produto()
    {
        //Construtor default
    }

    public Produto(Long id, TipoConsumo tipoConsumo)
    {
        super(id, tipoConsumo);
    }
    
    public Produto(String id, String descricao, TipoConsumo tipoConsumo)
    {
        super(id, descricao, tipoConsumo);
    }
    
    public Produto(BigDecimal estoque, Categoria categoria)
    {
        this.estoque = estoque;
        this.categoria = categoria;
    }
    
    public BigDecimal getEstoque()
    {
        return estoque;
    }

    public void setEstoque(BigDecimal estoque)
    {
        this.estoque = estoque;
    }
}