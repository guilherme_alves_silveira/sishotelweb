package br.com.jegss.model.po;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "categoria")
@Cacheable(true)
@Cache(type = CacheType.SOFT)
@NamedQueries(
        {
            @NamedQuery(name = "Categoria.findAll",
                        query = "SELECT c FROM Categoria c"),
            @NamedQuery(name = "Categoria.findLastNumber",
                        query = "SELECT MAX(c.id) + 1 FROM Categoria c")
        })
public class Categoria extends Entidade<Short>
{

    @Column(name = "descricao",
            length = 50,
            nullable = false)
    private String descricao;

    public Categoria()
    {
        //Construtor default
    }

    public Categoria(String descricao)
    {
        this.setDescricao(descricao);
    }

    public Categoria(Short id, String descricao)
    {
        this.setId(id);
        this.setDescricao(descricao);
    }

    @NotBlank(message = "descrição da categoria.")
    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    @Override
    public String toString()
    {
        return descricao;
    }
}
