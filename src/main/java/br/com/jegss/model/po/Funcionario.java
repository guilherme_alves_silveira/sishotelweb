package br.com.jegss.model.po;

import br.com.jegss.model.vo.Dinheiro;
import java.math.BigDecimal;
import java.util.Optional;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "funcionario")
@NamedQueries(
        {
            @NamedQuery(name = "Funcionario.findFuncionarioComEnderecoById",
                        query = "SELECT f FROM Funcionario f "
                        + "JOIN f.endereco e WHERE f.id = :id"),
            @NamedQuery(name = "Funcionario.findAll",
                        query = "SELECT f FROM Funcionario f"),
            @NamedQuery(name = "Funcionario.findLastNumber",
                        query = "SELECT MAX(f.id) + 1 FROM Funcionario f"),
            @NamedQuery(name = "Funcionario.findAllConstructor",
                        query = "SELECT NEW br.com.jegss.model.po.Funcionario(f.id, f.nome, f.cpf, f.rg) FROM Funcionario f")
        })
public class Funcionario extends Pessoa<EnderecoFuncionario>
{

    @Column(name = "salario",
            precision = 10,
            scale = 8)
    private BigDecimal salario;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_funcao",
                nullable = false)
    private Funcao funcao;

    @Column(name = "cargo",
            length = 50)
    private String cargo;

    @Column(name = "hora_entrada")
    private Integer horaEntrada;

    @Column(name = "hora_saida")
    private Integer horaSaida;

    @Column(name = "login",
            length = 50)
    private String login;

    @Column(name = "senha",
            length = 50)
    private String senha;

    @OneToOne(fetch = FetchType.LAZY,
              cascade = CascadeType.ALL,
              mappedBy = "funcionario")
    private EnderecoFuncionario endereco;

    public Funcionario()
    {
        //Construtor padrão
    }

    public Funcionario(Long id)
    {
        this.setId(id);
    }

    public Funcionario(String id, String nome, String cpf, String rg)
    {
        this(Long.parseLong(id));
        this.setNome(nome);
        this.setCpf(cpf);
        this.setRg(rg);
    }

    public Funcionario(String login, String senha)
    {
        this.login = login;
        this.senha = senha;
    }

    public Funcionario(BigDecimal salario, Funcao funcao,
                       String cargo, Integer horaEntrada,
                       Integer horaSaida)
    {
        this.salario = salario;
        this.funcao = funcao;
        this.cargo = cargo;
        this.horaEntrada = horaEntrada;
        this.horaSaida = horaSaida;
    }

    public Funcionario(BigDecimal salario, Funcao funcao,
                       String cargo, Integer horaEntrada,
                       Integer horaSaida, String login,
                       String senha)
    {
        this(login, senha);
        this.salario = salario;
        this.funcao = funcao;
        this.cargo = cargo;
        this.horaEntrada = horaEntrada;
        this.horaSaida = horaSaida;
    }

    @Override
    public EnderecoFuncionario getEndereco()
    {
        return endereco;
    }

    public Funcionario setEndereco(EnderecoFuncionario endereco)
    {
        this.endereco = endereco;
        return this;
    }

    public BigDecimal getSalario()
    {
        return salario;
    }

    public Funcionario setSalario(BigDecimal salario)
    {
        if (salario.doubleValue() < 0)
        {
            throw new IllegalArgumentException(
                    "O salário não pode ser menor que zero.");
        }
        else
        {
            this.salario = salario;
        }
        return this;
    }

    public Funcionario setDinheiro(Dinheiro dinheiro)
    {
        this.salario = dinheiro.getValor();
        return this;
    }

    public BigDecimal getDinheiro()
    {
        return getSalario();
    }

    public Funcao getFuncao()
    {
        return funcao;
    }

    public Funcionario setFuncao(Funcao funcao)
    {
        this.funcao = funcao;
        return this;
    }

    public String getCargo()
    {
        return cargo;
    }

    public Funcionario setCargo(String cargo)
    {
        this.cargo = cargo;
        return this;
    }

    public int getHoraEntrada()
    {
        return Optional.ofNullable(horaEntrada).orElse(0);
    }

    public Funcionario setHoraEntrada(Integer horaEntrada)
    {
        this.horaEntrada = horaEntrada;
        return this;
    }

    public int getHoraSaida()
    {
        return Optional.ofNullable(horaSaida).orElse(0);
    }

    public Funcionario setHoraSaida(Integer horaSaida)
    {
        this.horaSaida = horaSaida;
        return this;
    }

    public String getLogin()
    {
        return login;
    }

    public Funcionario setLogin(String login)
    {
        this.login = login;
        return this;
    }

    public String getSenha()
    {
        return senha;
    }

    public Funcionario setSenha(String senha)
    {
        this.senha = senha;
        return this;
    }
}
