package br.com.jegss.model.po;

import br.com.jegss.util.SistemaUtils;
import java.math.BigDecimal;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "consumo_unitario")
public class ConsumoUnitario extends Entidade<Long> implements Runnable
{
    private transient boolean isCalculado = false;
    private transient boolean isCalculadoComDesconto = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_bordero", 
                nullable = false)
    private Bordero bordero;

    @JoinColumn(name = "id_cabecalho", 
                referencedColumnName = "id",
                nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ConsumoCabecalho cabecalho;

    @Column(name = "quantidade",
            precision = 10,
            scale = 8, 
            nullable = false)
    private BigDecimal quantidade;

    @Column(name = "valor",
            precision = 10,
            scale = 8,
            nullable = false)
    private BigDecimal valor;

    @Column(name = "total",
            precision = 10,
            scale = 8,
            nullable = false)
    private BigDecimal total;

    @Column(name = "total_com_desconto", 
            precision = 10,
            scale = 8,
            nullable = false)
    private BigDecimal totalComDesconto;

    @Column(name = "desconto",
            precision = 10,
            scale = 8)
    private BigDecimal desconto;

    public ConsumoUnitario()
    {
        this.desconto = BigDecimal.ZERO;
        this.total = BigDecimal.ZERO;
        this.totalComDesconto = BigDecimal.ZERO;
        this.valor = BigDecimal.ZERO;
        this.quantidade = BigDecimal.ZERO;
    }

    public ConsumoUnitario(Bordero bordero, ConsumoCabecalho cabecalho,
                           BigDecimal quantidade, BigDecimal valor)
    {
        this();
        this.bordero = bordero;
        this.cabecalho = cabecalho;
        this.quantidade = quantidade;
        this.valor = valor;
    }

    @NotNull(message = "bordero")
    public Bordero getBordero()
    {
        return bordero;
    }

    public void setBordero(Bordero bordero)
    {
        this.bordero = bordero;
    }

    @NotNull(message = "cabeçalho")
    public ConsumoCabecalho getCabecalho()
    {
        return cabecalho;
    }

    public void setCabecalho(ConsumoCabecalho cabecalho)
    {
        this.cabecalho = cabecalho;
    }

    public BigDecimal getQuantidade()
    {
        return quantidade;
    }

    public ConsumoUnitario setQuantidade(BigDecimal quantidade)
    {
        if (quantidade.doubleValue() > 0)
        {
            this.quantidade = quantidade;
            this.isCalculado = false;
        }
        else
        {
            throw new IllegalArgumentException("A quantidade deve ser maior que zero.");
        }
        
        return this;
    }

    public BigDecimal getValor()
    {
        return valor;
    }

    public ConsumoUnitario setValor(BigDecimal valor)
    {
        if (valor.doubleValue() > 0)
        {
            this.valor = valor;
            this.isCalculado = false;
        }
        else
        {
            throw new IllegalArgumentException("O valor deve ser maior que zero.");
        }
        
        return this;
    }

    public BigDecimal getTotal()
    {
        if (!isCalculado)
        {
            calculaTotal();
        }
        return total;
    }

    public BigDecimal getTotalComDesconto()
    {
        if (!isCalculadoComDesconto)
        {
            calculaTotalComDesconto();
        }
        return totalComDesconto;
    }

    public BigDecimal getDesconto()
    {
        return desconto;
    }

    public ConsumoUnitario setDesconto(BigDecimal desconto)
    {
        if (desconto.doubleValue() >= 0)
        {
            this.desconto = desconto;
            this.isCalculadoComDesconto = false;
        }
        else
        {
            throw new IllegalArgumentException("O desconto deve ser maior que zero.");
        }
        
        return this;
    }

    @Override
    public void run()
    {
        calcula();
    }

    public void calcula()
    {
        calculaTotal();
        calculaTotalComDesconto();
    }

    private void calculaTotal()
    {
        //Total = Valor * Quantidade
        if (quantidade.doubleValue() > 0 && valor.doubleValue() > 0)
        {
            total = valor.multiply(quantidade);
            this.isCalculado = true;
        }
    }

    private void calculaTotalComDesconto()
    {
        BigDecimal valorPermitido = SistemaUtils.valorPermitidoPeloSistemaDeAcordoComPorcentagem(total);
        if (desconto.doubleValue() > 0 && desconto.doubleValue() < valorPermitido.doubleValue())
        {
            this.totalComDesconto = total.subtract(desconto);
        }
        else
        {
            this.totalComDesconto = total;
        }
        this.isCalculadoComDesconto = true;
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        long codigo = this.bordero != null? this.bordero.getId() : 0L;
        hash = 37 * hash + Objects.hashCode(codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final ConsumoUnitario other = (ConsumoUnitario) obj;
        return Objects.equals(this.bordero, other.bordero);
    }
}