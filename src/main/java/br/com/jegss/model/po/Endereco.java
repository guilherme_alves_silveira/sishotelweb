package br.com.jegss.model.po;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorColumn(name = "tipo_endereco", 
                     discriminatorType = DiscriminatorType.STRING)
public abstract class Endereco<T extends Pessoa> extends Entidade<Long>
{
    @Column(name = "numero",
            nullable = false)
    private Integer numero;
    
    @Column(name = "cep",
            length = 50)
    private String cep;
    
    @Column(name = "rua",
            length = 50,
            nullable = false)
    private String rua;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cidade",
                referencedColumnName = "id",
                nullable = false)
    private Cidade cidade;
    
    @Column(name = "bairro",
            length = 50,
            nullable = false)
    private String bairro;
    
    @Column(name = "complemento",
            length = 50)
    private String complemento;

    @NotNull(message = "Informe o número.")
    public Integer getNumero()
    {
        return numero;
    }

    public void setNumero(Integer numero)
    {
        this.numero = numero;
    }

    public String getCep()
    {
        return cep;
    }

    public void setCep(String cep)
    {
        this.cep = cep;
    }
    
    @NotBlank(message = "Informe a rua.")
    public String getRua()
    {
        return rua;
    }

    public void setRua(String rua)
    {
        this.rua = rua;
    }

    @NotNull(message = "Informe a cidade.")
    public Cidade getCidade()
    {
        return cidade;
    }

    public void setCidade(Cidade cidade)
    {
        this.cidade = cidade;
    }

    @NotBlank(message = "Informe o bairro.")
    public String getBairro()
    {
        return bairro;
    }

    public void setBairro(String bairro)
    {
        this.bairro = bairro;
    }

    public String getComplemento()
    {
        return complemento;
    }

    public void setComplemento(String complemento)
    {
        this.complemento = complemento;
    }
    
    public abstract void setPessoa(T pessoa);
}