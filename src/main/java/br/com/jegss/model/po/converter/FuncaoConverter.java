package br.com.jegss.model.po.converter;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.converter.Converter;
import br.com.jegss.model.po.Funcao;

@Convert(Funcao.class)
public class FuncaoConverter implements Converter<Funcao>
{
    @Override
    public Funcao convert(String value, Class<? extends Funcao> type)
    {
        try
        {
            Funcao funcao = new Funcao();
            Short id = Short.valueOf(value);
            funcao.setId(id);
            return funcao;
        }
        catch (Exception e)
        {
            return null;
        }
    }
}