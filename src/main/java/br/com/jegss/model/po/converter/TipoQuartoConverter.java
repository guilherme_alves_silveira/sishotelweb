package br.com.jegss.model.po.converter;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.converter.Converter;
import br.com.jegss.model.po.enumerated.TipoQuarto;

@Convert(TipoQuarto.class)
public class TipoQuartoConverter implements Converter<TipoQuarto>
{

    @Override
    public TipoQuarto convert(String value, Class<? extends TipoQuarto> type)
    {
        if (!"".equals(value))
        {
            value = value.trim();
        }

        switch (value)
        {
            case "Solteiro":
                return TipoQuarto.SOLTEIRO;
            case "SOLTEIRO":
                return TipoQuarto.SOLTEIRO;
            case "Casal":
                return TipoQuarto.CASAL;
            case "CASAL":
                return TipoQuarto.CASAL;
            case "Família":
                return TipoQuarto.FAMILIA;
            case "FAMILIA":
                return TipoQuarto.FAMILIA;
            case "FAMÍLIA":
                return TipoQuarto.FAMILIA;
            default:
                return TipoQuarto.SOLTEIRO;
        }
    }
}
