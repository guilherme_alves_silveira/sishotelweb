package br.com.jegss.model.po;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "funcao")
@NamedQuery(name = "Funcao.findAll", query = "SELECT f FROM Funcao f")
public class Funcao extends Entidade<Short>
{
    @Column(name = "descricao", 
            length = 50,
            nullable = false)
    private String descricao;

    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    @Override
    public String toString()
    {
        return descricao;
    }
}