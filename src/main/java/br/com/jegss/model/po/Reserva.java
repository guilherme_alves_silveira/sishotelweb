package br.com.jegss.model.po;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "reserva")
@NamedQueries(
{
    @NamedQuery(name = "Reserva.findWhere", query = "SELECT r FROM Reserva r WHERE r.id = :id"),
    @NamedQuery(name = "Reserva.findAll", query = "SELECT r FROM Reserva r INNER JOIN r.hospede h"),
    @NamedQuery(name = "Reserva.findAllConstructor", query = "SELECT NEW br.com.jegss.model.po.Reserva(r.id, h.id, h.nome, r.quarto, r.dataReserva) FROM Reserva r INNER JOIN r.hospede h"),
    @NamedQuery(name = "Reserva.findReservasByQuarto", query = "SELECT r FROM Reserva r WHERE r.quarto = :quarto"),
    @NamedQuery(name = "Reserva.findBetweenDataReserva", query = "SELECT r FROM Reserva r WHERE r.dataReserva BETWEEN :dataInicio AND :dataFim"),
    @NamedQuery(name = "Reserva.findReservasByHospedeWhereDataBetween", query = "SELECT r FROM Reserva r "
                + "WHERE r.hospede = :hospede AND "
                + "r.dataReserva BETWEEN :dataInicio AND :dataFim"),
    @NamedQuery(name = "Reserva.findHospedeWhoHasReserva", query = "SELECT r.hospede FROM Reserva r"),
})
public class Reserva extends Entidade<Long>
{
    @ManyToOne
    @JoinColumn(name = "id_hospede",
                referencedColumnName = "id",
                nullable = false)
    private Hospede hospede;

    @ManyToOne
    @JoinColumn(name = "id_quarto",
                referencedColumnName = "id",
                nullable = false)
    private Quarto quarto;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_reserva",
            nullable = false)
    private Calendar dataReserva;

    public Reserva()
    {
    }
    
    public Reserva(Long id, String idHospede, String nomeHospede, Quarto quarto, Calendar dataReserva)
    {
        this.setId(id);
        this.hospede = new Hospede(Long.valueOf(idHospede));
        this.hospede.setNome(nomeHospede);
        this.quarto = quarto;
        this.dataReserva = dataReserva;
    }
    
    public Reserva(Hospede hospede, Quarto quarto, Calendar dataReserva)
    {
        this.hospede = hospede;
        this.quarto = quarto;
        this.dataReserva = dataReserva;
    }

    public Hospede getHospede()
    {
        return hospede;
    }

    public void setHospede(Hospede hospede)
    {
        this.hospede = hospede;
    }
    
    public void setHospede(Long idHospede)
    {
        this.hospede = new Hospede(idHospede);
    }
    
    public Quarto getQuarto()
    {
        return quarto;
    }

    public void setQuarto(Quarto quarto)
    {
        this.quarto = quarto;
    }

    public Calendar getDataReserva()
    {
        return dataReserva;
    }

    public void setDataReserva(Calendar dataReserva)
    {
        this.dataReserva = dataReserva;
    }
}