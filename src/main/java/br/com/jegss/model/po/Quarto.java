package br.com.jegss.model.po;

import br.com.jegss.model.po.enumerated.TipoQuarto;
import br.com.jegss.model.vo.Dinheiro;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "quarto")
@NamedQueries(
        {
            @NamedQuery(name = "Quarto.findAllConstructor", query = "SELECT NEW br.com.jegss.model.po.Quarto(q.id, q.numero, q.valor) FROM Quarto q")
        })
public class Quarto extends Entidade<Short> implements Serializable
{

    @Column(name = "numero",
            nullable = false)
    private Integer numero;

    @Column(name = "valor",
            precision = 10,
            scale = 8,
            nullable = false)
    private BigDecimal valor;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_quarto",
            nullable = false)
    private TipoQuarto tipoQuarto;

    public Quarto()
    {

    }

    public Quarto(Short id, Integer numero, BigDecimal valor)
    {
        this.setId(id);
        this.setNumero(numero);
        this.setValor(valor);
    }

    public int getNumero()
    {
        return numero;
    }

    public void setNumero(int numero)
    {
        this.numero = numero;
    }

    public BigDecimal getValor()
    {
        return valor;
    }

    public void setValor(BigDecimal valor)
    {
        this.valor = valor;
    }
    
    public void setDinheiro(Dinheiro dinheiro)
    {
        this.valor = dinheiro.getValor();
    }
    
    public BigDecimal getDinheiro()
    {
        return getValor();
    }

    public TipoQuarto getTipoQuarto()
    {
        return tipoQuarto;
    }

    public void setTipoQuarto(TipoQuarto tipoQuarto)
    {
        this.tipoQuarto = tipoQuarto;
    }
}
