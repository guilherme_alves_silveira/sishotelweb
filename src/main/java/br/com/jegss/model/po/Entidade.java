package br.com.jegss.model.po;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Entidade<E extends Number> implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected E id;

    public E getId()
    {
        return id;
    }

    public void setId(E id)
    {
        this.id = id;
    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Entidade<?> other = (Entidade<?>) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString()
    {
        return getId() != null? getId().toString() : "0";
    }
}