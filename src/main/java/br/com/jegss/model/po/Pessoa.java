package br.com.jegss.model.po;

import br.com.jegss.model.po.enumerated.Sexo;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.br.CPF;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Pessoa<T extends Endereco> extends Entidade<Long>
{
    @Column(name = "nome",
            length = 100,
            nullable = false)
    private String nome;
    
    @Column(name = "cpf",
            length = 14)
    private String cpf;

    @Column(name = "rg",
            length = 14)
    private String rg;

    @Column(name = "passaporte",
            length = 50)
    private String passaporte;

    @Temporal(TemporalType.DATE)
    @Column(name = "data_nascimento",
            nullable = false)
    private Calendar dataNascimento;

    @Enumerated(EnumType.STRING)
    @Column(name = "sexo")
    private Sexo sexo;

    @NotBlank(message = "Preencha o nome.")
    public String getNome()
    {
        return nome;
    }

    @CPF(message = "Informe um CPF válido.")
    public String getCpf()
    {
        return cpf;
    }

    public String getRg()
    {
        return rg;
    }

    @NotNull(message = "Informe a data de nascimento.")
    public Calendar getDataNascimento()
    {
        return dataNascimento;
    }

    @NotNull(message = "Informe o sexo.")
    public Sexo getSexo()
    {
        return sexo;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public void setCpf(String CPF)
    {
        this.cpf = CPF;
    }

    public void setRg(String rg)
    {
        this.rg = rg;
    }

    public void setDataNascimento(Calendar dataNascimento)
    {
        this.dataNascimento = dataNascimento;
    }

    public void setSexo(Sexo sexo)
    {
        this.sexo = sexo;
    }

    public String getPassaporte()
    {
        return passaporte;
    }

    public void setPassaporte(String passaporte)
    {
        this.passaporte = passaporte;
    }
    
    public abstract T getEndereco();

    @Override
    public String toString()
    {
        return "Pessoa{" + "nome=" + nome + ", cpf=" + cpf +
                ", rg=" + rg + ", passaporte=" + passaporte +
                ", dataNascimento=" + new SimpleDateFormat("dd/MM/yyyy").format(dataNascimento != null? dataNascimento.getTime() : Calendar.getInstance().getTime() ) +
                ", sexo=" + (sexo != null? sexo.getDescricao() : "SEXO NULO") + '}';
    }
}