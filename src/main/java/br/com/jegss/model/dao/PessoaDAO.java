/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jegss.model.dao;

import br.com.jegss.model.po.Endereco;
import br.com.jegss.model.po.Pessoa;
import com.google.common.base.Strings;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

public abstract class PessoaDAO<T extends Pessoa, V extends Endereco> extends GenericoDAO<T, Long>
{

    private final Class<V> classeEndereco;

    public PessoaDAO(EntityManager em, Class<T> classeEntidade, Class<V> classeEndereco)
    {
        super(em, classeEntidade);
        this.classeEndereco = classeEndereco;
    }

    public Long getEnderecoID(T objPessoa)
    {
        final String endereco = classeEndereco.getSimpleName();
        final String pessoa = getClasseEntidade().getSimpleName().toLowerCase();
        final String jpql = String.format("SELECT e.id FROM %s e WHERE e.%s = :pessoa", endereco, pessoa);
        Query query = getEntityManager().createQuery(jpql);
        query.setParameter("pessoa", objPessoa);
        Object temp = query.getSingleResult();
        return (temp instanceof String) ? Long.valueOf((String) temp) : (Long) temp;
    }

    public boolean existePessoaComMesmoCPF(String cpf, Long id)
    {
        if (Strings.isNullOrEmpty(cpf))
        {
            throw new IllegalArgumentException("O CPF não pode estar nulo ou vazio.");
        }

        try
        {
            final String pessoa = getClasseEntidade().getSimpleName();
            final String jpql = String.format("SELECT e.cpf FROM %s e WHERE e.cpf = :cpf AND e.id <> :id", pessoa);
            TypedQuery<String> query = getEntityManager().createQuery(jpql, String.class);
            query.setParameter("cpf", cpf)
                 .setParameter("id", id);
            return !query.getSingleResult().isEmpty();
        }
        catch (EntityNotFoundException e)
        {
            return false;
        }
    }

}
