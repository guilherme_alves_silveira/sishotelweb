package br.com.jegss.model.dao;

import br.com.jegss.model.po.Produto;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class ProdutoDAO extends BorderoDAO<Produto>
{
    @Inject
    public ProdutoDAO(EntityManager em)
    {
        super(em, Produto.class);
    }

    @Deprecated
    public ProdutoDAO()
    {
        super(null, null);
    }
    
    @Override
    public List<Produto> listaTodos()
    {
        return getEntityManager().createNamedQuery("Produto.findAll")
                                 .getResultList();
    }
}