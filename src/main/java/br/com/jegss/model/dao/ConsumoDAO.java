package br.com.jegss.model.dao;

import br.com.jegss.model.po.ConsumoCabecalho;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class ConsumoDAO extends GenericoDAO<ConsumoCabecalho, Long>
{
    @Inject
    public ConsumoDAO(EntityManager em)
    {
        super(em, ConsumoCabecalho.class);
    }

    @Deprecated
    public ConsumoDAO()
    {
        super(null, null);
    }
    
    @Override
    public List<ConsumoCabecalho> listaTodos()
    {
        return getEntityManager().createNamedQuery("ConsumoCabecalho.findAll")
                                 .getResultList();
    }
}