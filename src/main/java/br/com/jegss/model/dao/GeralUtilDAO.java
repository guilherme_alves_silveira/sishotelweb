package br.com.jegss.model.dao;

import br.com.jegss.model.po.Categoria;
import br.com.jegss.model.po.Cidade;
import br.com.jegss.model.po.Estado;
import br.com.jegss.model.po.Funcao;
import br.com.jegss.model.po.Pais;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.CacheRetrieveMode;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class GeralUtilDAO
{
    private static EntityManager em;
    private static EnderecoUtilDAOFiltro filtroDAO;
    private static List<Funcao> funcaofindAllConstructor;
    private static List<Estado> estadofindAllConstructor;
    private static Long quantidadeDePaises;

    @Inject
    public GeralUtilDAO(EntityManager em, EnderecoUtilDAOFiltro filtroDAO)
    {
        GeralUtilDAO.em = em;
        GeralUtilDAO.filtroDAO = filtroDAO;
    }

    @Deprecated
    public GeralUtilDAO()
    {
        this(null, null);
    }

    public List<Estado> listaEstados()
    {
        if (estadofindAllConstructor == null || estadofindAllConstructor.isEmpty())
        {
            TypedQuery<Estado> readQuery = em.createNamedQuery("Estado.findAllConstructor", Estado.class);
            readQuery.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.USE);
            estadofindAllConstructor = readQuery.getResultList();
        }
        return estadofindAllConstructor;
    }

    public List<Funcao> listaFuncoesDeFuncionario()
    {
        if (funcaofindAllConstructor == null || funcaofindAllConstructor.isEmpty())
        {
            TypedQuery<Funcao> readQuery = em.createNamedQuery("Funcao.findAll", Funcao.class);
            readQuery.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.USE);
            funcaofindAllConstructor = readQuery.getResultList();
        }
        return funcaofindAllConstructor;
    }

    public List<Categoria> listaCategorias()
    {
        TypedQuery<Categoria> readQuery = em.createNamedQuery("Categoria.findAll", Categoria.class);
        readQuery.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.USE);
        return readQuery.getResultList();
    }

    public List<Cidade> listaCidadesDeAcordoComOEstadoSelecionado(String uf)
    {
        TypedQuery<Cidade> readQuery = em.createNamedQuery("Cidade.findAllConstructorByUF", Cidade.class);
        readQuery.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.USE);
        readQuery.setParameter("uf", uf);
        return readQuery.getResultList();
    }

    public List<Pais> buscaPaginadaDePaises(int primeiroResultado, int quantidadeDeResultados)
    {
        TypedQuery<Pais> readQuery = em.createNamedQuery("Pais.findAllPaises", Pais.class);
        readQuery.setFirstResult(primeiroResultado)
                .setMaxResults(quantidadeDeResultados);
        return readQuery.getResultList();
    }

    public Long buscaTotalDePais()
    {
        if (quantidadeDePaises == null || quantidadeDePaises <= 0)
        {
            TypedQuery<Long> readQuery = em.createQuery("SELECT COUNT(p) FROM Pais p", Long.class);
            quantidadeDePaises = readQuery.getSingleResult();
        }
        return quantidadeDePaises;
    }

    public List<Pais> buscaListaFiltradaDePaises(Pais entidade, String... propriedades)
    {
        return filtroDAO.buscaListaFiltrada(entidade, propriedades);
    }

    public Estado buscaEstado(Short id)
    {
        return em.find(Estado.class, id);
    }

    public static class EnderecoUtilDAOFiltro extends GenericoDAO<Pais, Short>
    {

        @Inject
        public EnderecoUtilDAOFiltro(EntityManager em)
        {
            super(em, Pais.class);
        }

        @Deprecated
        public EnderecoUtilDAOFiltro()
        {
            this(null);
        }

        @Override
        public List listaTodos()
        {
            return em.createNamedQuery("Pais.findAllPaises", Pais.class)
                    .getResultList();
        }

        public List<Pais> buscaListaFiltrada(Pais entidade, String... propriedades)
        {
            return super.filtrar(entidade, propriedades);
        }
    }
}
