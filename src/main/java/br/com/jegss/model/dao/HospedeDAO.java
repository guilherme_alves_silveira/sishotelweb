package br.com.jegss.model.dao;

import br.com.jegss.model.po.EnderecoHospede;
import br.com.jegss.model.po.Hospede;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class HospedeDAO extends PessoaDAO<Hospede, EnderecoHospede>
{
    @Inject
    public HospedeDAO(EntityManager em)
    {
        super(em, Hospede.class, EnderecoHospede.class);
    }
    
    @Deprecated
    public HospedeDAO()
    {
        super(null, null, null);
    }
    
    @Override
    public List<Hospede> listaTodos()
    {
        return getEntityManager().createNamedQuery("Hospede.findAll")
                                 .getResultList();
    }
}