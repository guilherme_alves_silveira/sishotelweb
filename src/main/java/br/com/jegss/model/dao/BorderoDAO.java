package br.com.jegss.model.dao;

import br.com.jegss.model.po.Bordero;
import br.com.jegss.model.po.Despesa;
import br.com.jegss.model.po.Produto;
import br.com.jegss.model.po.Servico;
import br.com.jegss.model.po.enumerated.TipoConsumo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;

public class BorderoDAO<T extends Bordero> extends GenericoDAO<T, Long>
{
    public BorderoDAO(EntityManager em, Class<T> classeEntidade)
    {
        super(em, classeEntidade);
    }

    @Override
    public List<T> buscaPaginadaDeDados(int primeiroResultado, int quantidadeDeResultados)
    {
        List<Bordero> borderos = new ArrayList<>();
        try
        {
            String sql = "SELECT id, descricao, tipo_consumo FROM bordero b LIMIT ?, ?";
            try (PreparedStatement pstmt = getConnection().prepareStatement(sql))
            {
                pstmt.setInt(1, primeiroResultado);
                pstmt.setInt(2, quantidadeDeResultados);

                try (ResultSet rs = pstmt.executeQuery())
                {
                    while (rs.next())
                    {
                        Long id = rs.getLong("id");
                        String tipoConsumo = rs.getString("tipo_consumo");
                        String descricao = rs.getString("descricao");
                        //*****************************************
                        Bordero bordero = null;
                        bordero = instanciaTipoDeBordero(tipoConsumo, bordero);
                        bordero.setId(id);
                        bordero.setDescricao(descricao);
                        borderos.add(bordero);
                    }
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return (List<T>) borderos;
    }

    @Override
    public Long getUltimoNumeroDeEntidade()
    {
        Long ultimoNumero = 1L;

        try
        {
            String sql = "SELECT MAX(id) + 1 AS ultimo_numero FROM bordero";
            try (PreparedStatement pstmt = getConnection().prepareStatement(sql))
            {
                try (ResultSet rs = pstmt.executeQuery())
                {
                    if (rs.next())
                    {
                        ultimoNumero = rs.getLong("ultimo_numero");
                    }
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return ultimoNumero;
    }
    
    public synchronized TipoConsumo getTipoConsumoBordero(Long id)
    {
        try
        {
            String sql = "SELECT tipo_consumo FROM bordero WHERE id = ?";
            try (PreparedStatement pstmt = getConnection().prepareStatement(sql))
            {
                pstmt.setLong(1, id);
                try (ResultSet rs = pstmt.executeQuery())
                {
                    if (rs.next())
                    {
                        String tipoConsumo = rs.getString("tipo_consumo");
                        return retornaTipoConsumo(tipoConsumo);
                    }
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Long buscaTotalDeItens()
    {
        Long totalItens = 0L;

        try
        {
            String sql = "SELECT COUNT(*) AS total_itens FROM bordero";
            try (PreparedStatement pstmt = getConnection().prepareStatement(sql))
            {
                try (ResultSet rs = pstmt.executeQuery())
                {
                    if (rs.next())
                    {
                        totalItens = rs.getLong("total_itens");
                    }
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return totalItens;
    }

    @Override
    public List<T> listaTodos()
    {
        throw new UnsupportedOperationException("Função listaTodos não implementada!");
    }

    @Override
    public T pegaPorId(Long codigo)
    {
        T entidade = null;
        try
        {
            entidade = getEntityManager().find(getClasseEntidade(), codigo);
        }
        catch (EntityNotFoundException e)
        {
            //Não faz nada, pois a entidade não foi encontrada
        }
        return entidade;
    }
    
    private Bordero instanciaTipoDeBordero(String tipoConsumo, Bordero bordero)
    {
        if (TipoConsumo.PRODUTO.toString().equals(tipoConsumo))
        {
            bordero = new Produto();
            bordero.setTipoConsumo(TipoConsumo.PRODUTO);
        }
        else if (TipoConsumo.SERVICO.toString().equals(tipoConsumo))
        {
            bordero = new Servico();
            bordero.setTipoConsumo(TipoConsumo.SERVICO);
        }
        else if (TipoConsumo.DESPESA.toString().equals(tipoConsumo))
        {
            bordero = new Despesa();
            bordero.setTipoConsumo(TipoConsumo.DESPESA);
        }
        else
        {
            throw new IllegalArgumentException("O bordero informado é inválido!");
        }

        return bordero;
    }
    
    private synchronized static TipoConsumo retornaTipoConsumo(String tipoConsumo)
    {
        if (TipoConsumo.PRODUTO.toString().equals(tipoConsumo))
        {
            return TipoConsumo.PRODUTO;
        }
        else if (TipoConsumo.SERVICO.toString().equals(tipoConsumo))
        {
            return TipoConsumo.SERVICO;
        }
        else if (TipoConsumo.DESPESA.toString().equals(tipoConsumo))
        {
            return TipoConsumo.DESPESA;
        }
        else
        {
            return null;
        }
    }
}
