package br.com.jegss.model.dao;

import br.com.jegss.model.po.Categoria;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class CategoriaDAO extends GenericoDAO<Categoria, Short>
{
    @Inject
    public CategoriaDAO(EntityManager em)
    {
        super(em, Categoria.class);
    }

    @Deprecated
    public CategoriaDAO()
    {
        super(null, null);
    }
    
    @Override
    public List<Categoria> listaTodos()
    {
        return getEntityManager().createNamedQuery("Categoria.findAll")
                                 .getResultList();
    }
}