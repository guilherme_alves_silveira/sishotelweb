package br.com.jegss.model.dao;

import br.com.jegss.model.po.Reserva;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class ReservaDAO extends GenericoDAO<Reserva, Long>
{
    @Inject
    public ReservaDAO(EntityManager em)
    {
        super(em, Reserva.class);
    }

    @Deprecated
    public ReservaDAO()
    {
        super(null, null);
    }
    
    @Override
    public List<Reserva> listaTodos()
    {
        return getEntityManager().createNamedQuery("Reserva.findAll'")
                                 .getResultList();
    }
}