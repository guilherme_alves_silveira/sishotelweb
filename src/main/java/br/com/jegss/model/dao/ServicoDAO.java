package br.com.jegss.model.dao;

import br.com.jegss.model.po.Servico;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class ServicoDAO extends BorderoDAO<Servico>
{
    @Inject
    public ServicoDAO(EntityManager em)
    {
        super(em, Servico.class);
    }

    @Deprecated
    public ServicoDAO()
    {
        super(null, null);
    }
    
    @Override
    public List<Servico> listaTodos()
    {
        return getEntityManager().createNamedQuery("Servico.findAll")
                                 .getResultList();
    }
}