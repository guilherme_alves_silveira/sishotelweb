package br.com.jegss.model.dao;

import br.com.jegss.model.po.Entidade;
import java.math.BigInteger;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.beanutils.PropertyUtils;

public abstract class GenericoDAO<T extends Entidade, ID extends Number>
{

    private EntityManager em;
    Class<T> classeEntidade;

    public GenericoDAO(EntityManager em, Class<T> classeEntidade)
    {
        if (em != null && classeEntidade != null)
        {
            this.em = em;
            this.classeEntidade = classeEntidade;
        }
        else
        {
            throw new NullPointerException("O EntityManager não pode estar nulo.");
        }
    }

    public Connection getConnection()
    {
        em.getTransaction().begin();
        Connection con = em.unwrap(Connection.class);
        em.getTransaction().commit();
        return con;
    }

    public T getSalvaOuAtualiza(T entidade)
    {
        if (isNotNull(entidade))
        {
            return em.merge(entidade);
        }
        else
        {
            throw new NullPointerException("A entidade não pode estar nulo.");
        }
    }

    public void salvaOuAtualiza(T entidade)
    {
        if (isNotNull(entidade))
        {
            em.merge(entidade);
        }
        else
        {
            throw new NullPointerException("A entidade não pode estar nulo.");
        }
    }

    public void deleta(T entidade, boolean deveConverter)
    {
        if (isNotNull(entidade) && (entidade.getId() != null))
        {
            entidade = pegaPorId((ID) entidade.getId(), deveConverter);
            em.remove(entidade);
        }
        else
        {
            throw new NullPointerException("A entidade não pode estar nulo.");
        }
    }

    public void deleta(T entidade)
    {
        if (isNotNull(entidade) && (entidade.getId() != null))
        {
            entidade = pegaPorId((ID) entidade.getId());
            em.remove(entidade);
        }
        else
        {
            throw new NullPointerException("A entidade não pode estar nulo.");
        }
    }

    public T pegaPorId(ID codigo)
    {
        T entidade = null;
        try
        {
            entidade = em.find(classeEntidade, String.valueOf(codigo));
        }
        catch (EntityNotFoundException e)
        {
            //Não faz nada, pois a entidade não foi encontrada
        }
        return entidade;
    }

    public T pegaPorId(ID codigo, boolean deveConverter)
    {
        T entidade = null;
        try
        {
            if (deveConverter)
            {
                return pegaPorId(codigo);
            }
            entidade = em.find(classeEntidade, codigo);
        }
        catch (EntityNotFoundException e)
        {
            //Não faz nada, pois a entidade não foi encontrada
        }
        return entidade;
    }

    @SuppressWarnings("unchecked")
    public List<T> filtrar(T entidade, String... propriedades)
    {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<T> criteria = (CriteriaQuery<T>) builder.createQuery(classeEntidade);
        Root<T> root = (Root<T>) criteria.from(classeEntidade);
        List<Predicate> predicates = new ArrayList<>();
        if (propriedades != null && propriedades.length > 0)
        {
            for (String propriedade : propriedades)
            {
                try
                {
                    Object valor = PropertyUtils.getProperty(entidade, propriedade);
                    if (valor != null)
                    {
                        Predicate predicate = null;
                        if (isValidValueString(valor))
                        {
                            predicate = builder.like(root.<String>get(propriedade), "%" + (String) valor + "%");
                            predicates.add(predicate);
                        }
                        else if (isNotValueString(valor))
                        {
                            predicate = builder.equal(root.get(propriedade), valor);
                            predicates.add(predicate);
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException(e);
                }
            }
        }

        criteria.where(predicates.toArray(new Predicate[0]));
        Query query = em.createQuery(criteria);

        return query.getResultList();
    }

    public List<T> buscaPaginadaDeDados(int primeiroResultado, int quantidadeDeResultados)
    {
        try
        {
            TypedQuery<T> readQuery = em.createNamedQuery(classeEntidade.getSimpleName() + ".findAllConstructor", classeEntidade);
            return readQuery.setFirstResult(primeiroResultado)
                    .setMaxResults(quantidadeDeResultados)
                    .getResultList();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public Long getUltimoNumeroDeEntidade()
    {
        BigInteger ultimo = getEntityManager().createNamedQuery(classeEntidade.getSimpleName() + ".findLastNumber", BigInteger.class)
                .getSingleResult();
        if (ultimo == null)
        {
            ultimo = BigInteger.ONE;
        }
        return ultimo.longValue();
    }

    public Long buscaTotalDeItens()
    {
        String jpql = String.format("SELECT COUNT(o) FROM %s o", classeEntidade.getSimpleName());
        TypedQuery<Long> readQuery = em.createQuery(jpql, Long.class);
        Long quantidade = readQuery.getSingleResult();
        if (quantidade == null)
        {
            quantidade = 0L;
        }
        return quantidade;
    }

    private boolean isValidValueString(Object valor)
    {
        return valor instanceof String && !((String) valor).trim().equals("");
    }

    private boolean isNotValueString(Object valor)
    {
        return !(valor instanceof String);
    }

    public abstract List<T> listaTodos();

    private boolean isNotNull(T entidade)
    {
        return (entidade != null);
    }

    public EntityManager getEntityManager()
    {
        return em;
    }

    protected Class<T> getClasseEntidade()
    {
        return classeEntidade;
    }
}
