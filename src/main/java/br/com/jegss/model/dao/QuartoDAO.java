package br.com.jegss.model.dao;

import br.com.jegss.model.po.Quarto;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class QuartoDAO extends GenericoDAO<Quarto, Short>
{
    @Inject
    public QuartoDAO(EntityManager em)
    {
        super(em, Quarto.class);
    }
    
    @Deprecated
    public QuartoDAO()
    {
        super(null, null);
    }
    
    @Override
    public List<Quarto> listaTodos()
    {
        return getEntityManager().createNamedQuery("Quarto.findAll")
                                 .getResultList();
    }
}