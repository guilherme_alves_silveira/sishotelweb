package br.com.jegss.model.dao;

import br.com.jegss.model.po.TipoPagamento;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class TipoPagamentoDAO extends GenericoDAO<TipoPagamento, Short>
{
    @Inject
    public TipoPagamentoDAO(EntityManager em)
    {
        super(em, TipoPagamento.class);
    }
    
    @Deprecated
    public TipoPagamentoDAO()
    {
        super(null, null);
    }

    @Override
    public List<TipoPagamento> listaTodos()
    {
        return getEntityManager().createNamedQuery("TipoPagamento.findAll")
                                 .getResultList();
    }
}