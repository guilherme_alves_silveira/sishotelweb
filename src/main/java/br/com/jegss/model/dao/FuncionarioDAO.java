package br.com.jegss.model.dao;

import br.com.jegss.model.po.EnderecoFuncionario;
import br.com.jegss.model.po.Funcionario;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class FuncionarioDAO extends PessoaDAO<Funcionario, EnderecoFuncionario>
{
    @Inject
    public FuncionarioDAO(EntityManager em)
    {
        super(em, Funcionario.class, EnderecoFuncionario.class);
    }

    @Deprecated
    public FuncionarioDAO()
    {
        super(null, null, null);
    }
    
    @Override
    public List<Funcionario> listaTodos()
    {
        return getEntityManager().createNamedQuery("Funcionario.findAll")
                                 .getResultList();
    }
}