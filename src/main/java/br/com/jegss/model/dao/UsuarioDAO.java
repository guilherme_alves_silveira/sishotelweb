package br.com.jegss.model.dao;

import br.com.jegss.model.po.Usuario;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

public class UsuarioDAO
{

    private final EntityManager em;

    @Inject
    public UsuarioDAO(EntityManager em)
    {
        this.em = em;
    }

    public UsuarioDAO()
    {
        this(null);
    }

    public void adiciona(Usuario usuario)
    {
        em.getTransaction().begin();
        em.merge(usuario);
        em.getTransaction().commit();
    }

    public List<Usuario> lista()
    {
        TypedQuery<Usuario> query = em.createQuery("SELECT u FROM Usuario u", Usuario.class);
        return query.getResultList();
    }

    public void deleta(Usuario usuario)
    {
        em.getTransaction().begin();
        usuario = em.merge(usuario);
        em.remove(usuario);
        em.getTransaction().commit();
    }

    public Usuario busca(String login, String senha)
    {
        try
        {
            TypedQuery<Usuario> query = em.createQuery("SELECT u FROM Usuario u WHERE u.login = :login AND u.senha = :senha", Usuario.class);
            query.setParameter("login", login);
            query.setParameter("senha", senha);
            return query.getSingleResult();
        }
        catch (NoResultException e)
        {
            return null;
        }
    }
}
