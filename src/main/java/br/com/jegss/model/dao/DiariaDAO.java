package br.com.jegss.model.dao;

import br.com.jegss.model.po.Diaria;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class DiariaDAO extends GenericoDAO<Diaria, Long>
{
    @Inject
    public DiariaDAO(EntityManager em)
    {
        super(em, Diaria.class);
    }

    @Deprecated
    public DiariaDAO()
    {
        super(null, null);
    }
    
    @Override
    public List<Diaria> listaTodos()
    {
        return getEntityManager().createNamedQuery("Diaria.findAll")
                                 .getResultList();
    }
}