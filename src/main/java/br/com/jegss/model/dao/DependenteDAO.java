package br.com.jegss.model.dao;

import br.com.jegss.model.po.Dependente;
import br.com.jegss.model.po.EnderecoDependente;
import br.com.jegss.model.po.Hospede;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

public class DependenteDAO extends PessoaDAO<Dependente, EnderecoDependente>
{

    @Inject
    public DependenteDAO(EntityManager em)
    {
        super(em, Dependente.class, EnderecoDependente.class);
    }

    @Deprecated
    public DependenteDAO()
    {
        super(null, null, null);
    }

    @Override
    public List<Dependente> listaTodos()
    {
        return getEntityManager().createNamedQuery("Dependente.findAll")
                .getResultList();
    }

    public Long getResponsavelPeloCPF(String cpfResponsavel)
    {
        try
        {
            String tempId = getEntityManager().createQuery("SELECT h.id FROM Hospede h WHERE h.cpf = :cpf", String.class)
                    .setParameter("cpf", cpfResponsavel)
                    .getSingleResult();
            return Long.valueOf(tempId);
        }
        catch (NoResultException e)
        {
            return null;
        }
    }

    public void setResponsaveisNoDependente(Dependente dependente)
    {
        try
        {
            List<String> tempIds = getEntityManager().createQuery("SELECT h.id FROM Hospede h JOIN h.dependentes d WHERE d.id = :id", String.class)
                                                    .setParameter("id", dependente.getId())
                                                    .getResultList();
            for (String tempId : tempIds)
            {
                Long id = Long.valueOf(tempId);
                Hospede responsavel = new Hospede();
                responsavel.setId(id);
                dependente.adicionaResponsavel(responsavel);
            }
        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
    }
}
