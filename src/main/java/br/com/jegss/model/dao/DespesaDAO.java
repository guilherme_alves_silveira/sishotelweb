package br.com.jegss.model.dao;

import br.com.jegss.model.po.Despesa;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class DespesaDAO extends BorderoDAO<Despesa>
{
    @Inject
    public DespesaDAO(EntityManager em)
    {
        super(em, Despesa.class);
    }

    @Deprecated
    public DespesaDAO()
    {
        super(null, null);
    }
    
    @Override
    public List<Despesa> listaTodos()
    {
        return getEntityManager().createNamedQuery("Despesa.findAll")
                                 .getResultList();
    }
}