/* global ROOT, CONTROLLER */

var isNotNullOrUndf = function(data)
{
    return data !== undefined && data !== null;
};

/**
 * Altera o action do formulário e executado o submit.
 * @param {type} uri A uri que será colocado no action do formulário.
 * @param {type} method o elemento com o id method que deve ter o 
 * name como _method para que seja enviado com sucesso para o servidor.
 * @param {type} form O formulário que será executado a função.
 * @returns {undefined} void
 */
var submitOnAction = function (uri, method, form)
{
    form.attr("action", uri);
    inputMethod = $("#method");
    if (inputMethod !== undefined && inputMethod !== null)
    {
        inputMethod.val(method);
    }
    form.submit();
};

/**
 * Recebe a data no formato
 * 2015-05-30T00:00:00-03:00,
 * e transforma em 30/05/2015
 * @param {type} data Retorna a data no formato (dd/MM/yyyy)
 * @returns {String}
 */
var getDataFormatada = function (data, separador)
{
    var deveSepararPor = "-" || separador;
    var data = data.replace("T00:00:00", "").split(deveSepararPor);
    var dia = data[2].toString();
    var mes = data[1].toString();
    var ano = data[0].toString();

    if (dia.length < 2)
    {
        dia = "0" + dia;
    }

    if (mes.length < 2)
    {
        mes = "0" + mes;
    }

    return dia + "/" + mes + "/" + ano;
};

/**
 * Retorna o valor formatado no padrão R$ 100,00, sempre contendo
 * o ",00" no final. Caso seja retornado 100 será retornado R$ 100,00,
 * se for retornado 100.5 vai ficar R$ 100,50 e assim por diante.
 * @param {type} valor Valor monetário
 * @param {type} tipoMoeda O valor padrão é R$ do contrário
 * poderá ser informa U$... entre outros.
 * @returns {String}
 */
var getDinheiro = function (valor, tipoMoeda)
{
    if (!isNotNullOrUndf(valor))
    {
        console.log("Valor do dinheiro está nulo!");
        return "ERROR";
    }
    else if (!isNotNullOrUndf(tipoMoeda))
    {
        tipoMoeda = "R$ ";
    }
    else
    {
        tipoMoeda = tipoMoeda + " ";
    }

    var valorMoneyAndCent = valor.toString().split(".");
    var valorTratado = null;
    if (valorMoneyAndCent.length === 2)
    {
        var real = valorMoneyAndCent[0];
        var centavos = valorMoneyAndCent[1];
        if (centavos.length === 1)
        {
            centavos = centavos + "0";
        }
        valorTratado = tipoMoeda + real + "," + centavos;
    }
    else
    {
        valorTratado = tipoMoeda + valorMoneyAndCent[0] + ",00";
    }
    return valorTratado;
};

/**
 * Busca o valor no select ou combo-box e seleciona esse
 * valor no elemento.
 * @param {type} elementId O id do elemento (select ou combo-box)
 * @param {type} value Valor ou Texto a ser buscado.
 * @param {type} byText Deve ser passado true caso seja buscado pelo text do elemento
 * ou false caso seja buscado pelo value do elemento.
 * @returns {Boolean} false em caso que o elemento não tenha sido encontrado ou true caso o elemento tenha
 * sido encontrado
 */
var setSelectByValueOrText = function (elementId, value, byText)
{
    var element = document.getElementById(elementId);

    if (byText !== true && byText !== false)
    {
        console.log("O parâmetro byText deve ser true ou false.");
    }

    for (var idx = 0; idx < element.length; idx++)
    {
        if (byText)
        {
            if (element.options[idx].text === value)
            {
                element.options[idx].selected = true;
                return true;
            }
        }
        else
        {
            if (element.options[idx].value === value)
            {
                element.options[idx].selected = true;
                return true;
            }
        }
    }
    return false;
};

/**
 * Caso o atributo error da variável data
 * não seja null nem undefined, então é exibido a mensagem
 * que está na variável error e é retornado true informando que 
 * contém erro, caso contrário é retornado falso informando que 
 * não há erro.
 * @param {type} data
 * @returns {Boolean}
 */
var possuiErro = function (data)
{
    if (isNotNullOrUndf(data))
    {
        return true;
    }
    
    if (isNotNullOrUndf(data.error))
    {
        return true;
    }
    
    return false;
};

var limpaForm = function(form)
{
    if (!isNotNullOrUndf(form))
    {
        console.log("Formulário é nulo, passe um válido(JQuery).");
        return;
    }
    
    form.find("input").each(function()
    {
        $(this).val("");
    });
};

/**
 * Recebe o valores do tipo
 * M e retorna "Masculino",
 * F e retorna "Feminino",
 * O e retorna "Outro",
 * caso contrário é retornado "ERRO"
 * @param {type} sexo
 * @returns {String}
 */
var getSexo = function (sexo)
{
    switch (sexo)
    {
        case "M":
            return "Masculino";
        case "F":
            return "Feminino";
        case "O":
            return "Outro";
        default:
            return "ERRO";
    }
};

/**
 * Executa uma chamada ajax no servidor,
 * e em caso de erro é exibido um alert,
 * caso contrário é chamado a função preencheCampos
 * que preenche os campos da tela com o resultado
 * JSON retornado.
 * @param {type} preencheCampos Uma função
 * @returns {undefined} Não retorna nada
 */
var buscaRegistroPeloCodigo = function (preencheCampos)
{
    var campoId = $("#id");
    campoId.on("blur", function ()
    {
        if ((!isNotNullOrUndf(ROOT) || ROOT === "") &&
            (!isNotNullOrUndf(CONTROLLER) || CONTROLLER === ""))
        {
            console.log("O ROOT E\OU O CONTROLLER NÃO ESTÃO CRIADOS!");
        }

        var campoIdTratado = campoId.val().replace(/_+/, "");
        if (isNaN(campoIdTratado))
        {
            return;
        }

        $.ajax
        ({
            type: "GET",
            dataType: "json",
            url: ROOT + CONTROLLER + "/busca/" + campoIdTratado,
            success: function (data)
            {
                if (isNotNullOrUndf(data))
                {
                    if (isNotNullOrUndf(data.error))
                    {
                        limpaForm($("#formCadastro"));
                        return;
                    }
                    
                    preencheCampos(data, campoIdTratado);
                }
                else
                {
                    //Cuidado ao utilizar alert, o mesmo impede que o javascript execute.
                    alert("Não foi possível comunicar com o servidor.");
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    });
};