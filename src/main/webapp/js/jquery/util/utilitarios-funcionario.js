/* global ROOT */

$(document).ready(function ()
{
    var cbFuncoes = $("#funcao");

    var preencheCbFuncoes = function ()
    {
        $.ajax
        ({
            type: 'GET',
            dataType: 'json',
            url: ROOT + "geral/funcoes",
            success: function (data)
            {
                for (var prop in data)
                {
                    var funcao = "<option value='" + data[prop].id + "'>" + data[prop].descricao + "</option>";
                    cbFuncoes.prepend(funcao);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    };
    
    preencheCbFuncoes();
});