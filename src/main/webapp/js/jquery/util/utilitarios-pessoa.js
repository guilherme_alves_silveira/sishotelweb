/* global ROOT, CONTROLLER */
$(document).ready(function()
{
    var cbEstado = $("#uf");
    var cbCidade = $("#cidade");

    var preencheCbEstados = function()
    {
        $.ajax
        ({
            type: 'GET',
            dataType: 'json',
            url: ROOT + "geral/estados",
            success: function(data)
            {
                for (var prop in data)
                {
                    var estado = "<option value='" + data[prop].id + "'>" + data[prop].uf + "</option>";
                    cbEstado.prepend(estado);
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    };

    preencheCbEstados();

    cbEstado.on("change", function(event)
    {
        event.preventDefault();
        var estadoSelecionado = this.value;
        $.ajax
        ({
            type: 'GET',
            dataType: 'json',
            url: ROOT + "geral/cidade/" + estadoSelecionado,
            success: function(data)
            {
                cbCidade.empty();
                for (var prop in data)
                {
                    var cidade = "<option value='" + data[prop].id + "'>" + data[prop].nome + "</option>";
                    cbCidade.prepend(cidade);
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    });
});