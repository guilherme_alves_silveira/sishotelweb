/* global ROOT, CONTROLLER */

/**
 * Se clicado no botão salvar, a uri é modificada
 * e a ação é executada no método salvar.
 */
$("#btnSalvar").on("click", function (event)
{
    event.preventDefault();
    var uriSalvarOuAlterar = ROOT + CONTROLLER;
    var possuiCodigo = !isNaN($("#id").val()) && !("" === $("#id").val());
    var method;
    if (possuiCodigo)
    {
        uriSalvarOuAlterar = uriSalvarOuAlterar + '/atualiza';
        method = "put";
    }
    else
    {
        uriSalvarOuAlterar = uriSalvarOuAlterar + '/salva';
        method = "post";
    }
    
    submitOnAction(uriSalvarOuAlterar, method, $("#formCadastro"));
});

/**
 * Se clicado no botão deletar, a uri é modificada
 * e a ação é executada no método deletar.
 */
$("#btnDeletar").on("click", function (event)
{
    event.preventDefault();
    var uriDeletar = ROOT + CONTROLLER + '/deleta';
    submitOnAction(uriDeletar, "delete", $("#formCadastro"));
});