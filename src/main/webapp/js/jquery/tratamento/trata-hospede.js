$(document).ready(function ($)
{
    $('#cpf').mask('999.999.999-99', {reverse: true});
    $("#cep").mask("99999-999");
    $("#dataNascimento").mask("99/99/9999");
});