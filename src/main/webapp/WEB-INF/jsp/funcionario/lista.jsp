<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="tableContainer" id="funcionarioTableContainer"></div>
<script>
    $(document).ready(function()
    {
        var table = $("#funcionarioTableContainer");
        table.jtable
        ({
            title: 'Lista de Funcion�rios',
            paging: true,
            pageSize: 30,
            sorting: true,
            defaultSorting: 'nome ASC',
            actions:{
              listAction: "<c:url value='/funcionario/lista' />"
            },
            fields:{
                id:{
                    title: 'C�digo',
                    list: true
                },
                nome:{
                    title: 'Nome',
                    list: true
                },
                cpf: {
                    title: 'CPF',
                    list: true
                }
            }
        });
        table.jtable('load');
    });
</script>