<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="v" %>

<form action="#" method="post" id="formCadastro">

    <input type="hidden" id="method" name="_method" value="post" />

    <div>
        <label for="id">C�digo:</label>
        <input type="text" class="form-control" id="id" name="id" value="${id}" />
        <v:validationMessage name="id" />
    </div>

    <div>
        <label for="nome">Nome:</label>
        <input type="text" class="form-control" id="nome" name="funcionario.nome" 
               data-validation="required" data-validation-error-msg="Campo obrigat�rio" value="${funcionario.nome}" />
        <v:validationMessage name="funcionario.nome" />
    </div>

    <div>
        <label for="cpf">CPF:</label>
        <input type="text" class="form-control" class="form-control" id="cpf" name="funcionario.cpf" value="${funcionario.cpf}" />
        <v:validationMessage name="funcionario.cpf" />
    </div>

    <div>
        <label for="rg">RG:</label>
        <input type="text" class="form-control" id="rg" name="funcionario.rg" value="${funcionario.rg}" 
               maxlength="14"/>
    </div>

    <div>
        <label for="passaporte">Passaporte:</label>
        <input type="text" class="form-control" id="passaporte" name="funcionario.passaporte" value="${funcionario.passaporte}" 
               maxlength="50"/>
    </div>

    <div>
        <label for="dataNascimento">Data Nascimento:</label>
        <input type="text" class="form-control" id="dataNascimento" name="funcionario.dataNascimento" value="${funcionario.dataNascimento}"/>
        <v:validationMessage name="funcionario.dataNascimento" />
    </div>

    <div>
        <label for="sexo">Sexo:</label>
        <select class="form-control" id="sexo" name="funcionario.sexo">
            <option value="M">Masculino</option>
            <option value="F">Feminino</option>
            <option value="O">Outro</option>
        </select>
        <v:validationMessage name="funcionario.sexo" />
    </div>

    <!-- Accordion de endere�o
    ======================-->
    <v:validationMessage name="funcionario.endereco" />
    <div class="col-md-12">
        <h2>Informa��es adicionais</h2>

        <div class="panel-group" id="accordion">

            <div class="panel panel-default">
                <!-- Cabe�alho (come�o)
                ==================-->
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#endereco">
                            Endere�o a ser cadastrado\atualizado
                        </a>
                    </h4>
                </div> <!-- Cabe�alho (fim) -->

                <!-- Corpo (come�o)
                ==================-->
                <div class="panel-collapse collapse in" id="endereco">
                    <div class="panel-body">

                        <div>
                            <label for="numero">N�mero:</label>
                            <input type="text" class="form-control" id="numero" name="endereco.numero" value="${endereco.numero}"/>
                            <v:validationMessage name="endereco.numero" />
                        </div>

                        <div>
                            <label for="cep">CEP:</label>
                            <input type="text" class="form-control" id="cep" name="endereco.cep" value="${endereco.cep}" />
                        </div>

                        <div>
                            <label for="rua">Rua:</label>
                            <input type="text" class="form-control" id="rua" name="endereco.rua" value="${endereco.rua}"/>
                            <v:validationMessage name="endereco.rua" />
                        </div>

                        <div>
                            <label for="bairro">Bairro</label>
                            <input type="text" class="form-control" id="bairro" name="endereco.bairro" value="${endereco.bairro}"/>
                            <v:validationMessage name="endereco.bairro" />
                        </div>

                        <div>
                            <label for="complemento">Complemento:</label>
                            <input type="text" class="form-control" id="complemento" name="endereco.complemento" value="${endereco.complemento}" />
                        </div>

                        <div>
                            <label for="uf">UF:</label>
                            <select class="form-control" id="uf" name="endereco.estado"/>
                            </select>
                        </div>

                        <div>
                            <label for="cidade">Cidade:</label>
                            <select class="form-control" id="cidade" name="endereco.cidade"/>
                            </select>
                            <v:validationMessage name="endereco.cidade" />
                        </div>

                    </div>
                </div><!-- Corpo (fim) -->

            </div>

            <div class="panel panel-default">

                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#dados">
                            Dados do funcion�rio
                        </a>
                    </h4>
                </div>

                <!-- Corpo (come�o)
                ==================-->
                <div class="panel-collapse collapse" id="dados">
                    <div class="panel-body">

                        <div>
                            <label for="salario">Sal�rio:</label>
                            <input class="form-control" id="salario" name="funcionario.dinheiro" value="${funcionario.salario}"/>
                            <v:validationMessage name="funcionario.salario" />
                        </div>
                        
                        <div>
                            <label for="funcao">Fun��o:</label>
                            <select class="form-control" id="funcao" name="funcionario.funcao"/>
                            </select>
                            <v:validationMessage name="funcionario.funcao" />
                        </div>
                        
                        <div>
                            <label for="cargo">Cargo:</label>
                            <input type="text" class="form-control" id="cargo" name="funcionario.cargo" value="${funcionario.cargo}"/>
                            <v:validationMessage name="funcionario.cargo" />
                        </div>
                        
                        <div>
                            <label for="horaEntrada">Hora de Entrada:</label>
                            <input type="text" class="form-control" id="horaEntrada" name="funcionario.horaEntrada" value="${funcionario.horaEntrada}"/>
                            <v:validationMessage name="funcionario.horaEntrada" />
                        </div>

                        <div>
                            <label for="horaSaida">Hora de Sa�da:</label>
                            <input type="text" class="form-control" id="horaSaida" name="funcionario.horaSaida" value="${funcionario.horaSaida}"/>
                            <v:validationMessage name="funcionario.horaSaida" />
                        </div>
                        
                        <div>
                            <label for="login">Login:</label>
                            <input type="text" class="form-control" id="login" name="funcionario.login" value="${funcionario.login}"/>
                            <v:validationMessage name="funcionario.login" />
                        </div>

                        <div>
                            <label for="senha">Senha:</label>
                            <input type="password" class="form-control" id="senha" name="funcionario.senha" value="${funcionario.senha}"/>
                            <v:validationMessage name="funcionario.senha" />
                        </div>
                        
                    </div><!-- Corpo (fim) -->
                </div>
                        
            </div>
                        
        </div>

    </div>

    <div class="panel">
        <input type="button" id="btnSalvar" value="Salvar" class="btn btn-primary" />
        <input type="button" id="btnDeletar" value="Deletar" class="btn btn-danger" />
        <input type="reset" value="Limpar tela" class="btn btn-default" />
    </div>

</form>

<script>
    ROOT = '<c:url value="/" />';
    CONTROLLER = 'funcionario';
</script>
<script src="<c:url value='/js/jquery/util/utilitarios.js' />"></script>
<script src="<c:url value='/js/jquery/util/utilitarios-pessoa.js' />"></script>
<script src="<c:url value='/js/jquery/util/utilitarios-funcionario.js' />"></script>
<script src="<c:url value='/js/jquery/util/eventos.js' />"></script>
<script>
    buscaRegistroPeloCodigo(function (data, campoIdTratado)
    {
        var pessoa = data;
        //Pessoa
        $("#id").val(campoIdTratado);
        $("#nome").val(pessoa.nome);
        $("#cpf").val(pessoa.cpf);
        $("#rg").val(pessoa.rg);
        $("#passaporte").val(pessoa.passaporte);
        $("#dataNascimento").val(getDataFormatada(pessoa.dataNascimento));
        setSelectByValueOrText("sexo", getSexo(pessoa.sexo), true);
    });
</script>