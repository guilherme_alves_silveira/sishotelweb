<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="tableContainer" id="hospedeTableContainer"></div>
<script>
    $(document).ready(function()
    {
        var table = $("#hospedeTableContainer");
        table.jtable
        ({
            title: 'Lista de Hospedes',
            paging: true,
            pageSize: 30,
            sorting: true,
            defaultSorting: 'nome ASC',
            actions:{
              listAction: "<c:url value='/hospede/lista' />"
            },
            fields:{
                id:{
                    title: 'C�digo',
                    list: true
                },
                nome:{
                    title: 'Nome',
                    list: true
                },
                cpf: {
                    title: 'CPF',
                    list: true
                }
            }
        });
        table.jtable('load');
    });
</script>