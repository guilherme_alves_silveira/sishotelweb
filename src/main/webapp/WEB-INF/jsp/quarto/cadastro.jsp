<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="v" %>

<form action="#" method="post" id="formCadastro">

    <input type="hidden" id="method" name="_method" value="post" />

    <div>
        <label for="id">C�digo:</label>
        <input type="text" class="form-control" id="id" name="id" value="${id}" />
        <v:validationMessage name="id" />
    </div>

    <div>
        <label for="numero">N�mero:</label>
        <input type="text" class="form-control" id="numero" name="quarto.numero" value="${quarto.numero}" />
        <v:validationMessage name="quarto.numero" />
    </div>

    <div>
        <label for="valor">Valor:</label>
        <input type="text" class="form-control" id="valor" name="quarto.dinheiro" value="${quarto.valor}" />
        <v:validationMessage name="quarto.valor" />
    </div>

    <div>
        <label for="tipoQuarto">Tipo do Quarto:</label>
        <select class="form-control" id="tipoQuarto" name="quarto.tipoQuarto">
            <option value="CASAL">Casal</option>
            <option value="SOLTEIRO">Solteiro</option>
            <option value="FAMILIA">Fam�lia</option>
        </select>
        <v:validationMessage name="quarto.tipoQuarto" />
    </div>

    <div>
        <input type="button" id="btnSalvar" value="Salvar" class="btn btn-primary" />
        <input type="button" id="btnDeletar" value="Deletar" class="btn btn-danger" />
        <input type="reset" value="Limpar tela" class="btn btn-default" />
    </div>

</form>

<script>
    ROOT = '<c:url value="/" />';
    CONTROLLER = 'quarto';
</script>
<script src="<c:url value='/js/jquery/util/utilitarios.js' />"></script>
<script src="<c:url value='/js/jquery/tratamento/trata-geral.js'/>"></script>
<script src="<c:url value='/js/jquery/tratamento/trata-quarto.js'/>"></script>
<script src="<c:url value='/js/jquery/util/eventos.js' />"></script>
<script>
    buscaRegistroPeloCodigo(function (data, campoIdTratado)
    {
        var quarto = data;
        $("#id").val(campoIdTratado);
        $("#numero").val(quarto.numero);
        $("#valor").val(getDinheiro(quarto.valor));
        setSelectByValueOrText("tipoQuarto", quarto.tipoQuarto, false);
    });
</script>