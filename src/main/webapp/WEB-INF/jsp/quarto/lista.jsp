<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="tableContainer" id="quartoTableContainer"></div>
<script>
    $(document).ready(function()
    {
        var table = $("#quartoTableContainer");
        table.jtable
        ({
            title: 'Lista de Quartos',
            paging: true,
            pageSize: 30,
            sorting: true,
            defaultSorting: 'id ASC',
            actions:{
              listAction: "<c:url value='/quarto/lista' />"
            },
            fields:{
                id:{
                    title: 'C�digo',
                    list: true
                },
                numero:{
                    title: 'N�mero',
                    list: true
                },
                valor:{
                    title: 'Valor',
                    list: true
                }
            }
        });
        table.jtable('load');
    });
</script>