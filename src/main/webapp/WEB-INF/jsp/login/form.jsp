<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="a" %>
<c:import url="/WEB-INF/jsp/header.jsp" />

<a:validationMessage name="login_invalido"/>
<form action="<c:url value='/login/autentica' />" method="post">
    
    <label for="login">Login:</label>
    <input type="text" name="login" id="login" class="form-control" />
    
    <label for="senha">Senha:</label>
    <input type="password" name="senha" id="senha" class="form-control" />
    
    <br/>
    <input type="submit" value="Login" class="btn btn-primary" />
    
</form>
<c:import url="/WEB-INF/jsp/footer.jsp" />