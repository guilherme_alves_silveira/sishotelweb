<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="/WEB-INF/jsp/header.jsp" />
<!-- Local onde ser� colocado o conte�do (cabe�alho, cadastro e listagem).
========================-->
<div class="row">
    <!-- Cabe�alho da se��o
    ============================-->
    <div class="col-md-12">
        <nav>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#cadastro" data-toggle="tab">Cadastro de Dependentes</a></li>
                <li><a href="#lista" data-toggle="tab">Listagem de Dependentes</a></li>
            </ul>
        </nav>
    </div>
    
    <script src="<c:url value='/js/jquery/tratamento/trata-geral.js'/>"></script>
    <script src="<c:url value='/js/jquery/tratamento/trata-hospede.js'/>"></script>
    <script src="<c:url value='/js/jquery/tratamento/trata-dependente.js'/>"></script>
    
    <!-- Conte�do da se��o
    ============================-->
    <div class="tab-content">
        <!-- Formul�rio de cadastro (come�o)
        =======================-->
        <div class="tab-pane active" id="cadastro">
            
            <c:import url="/WEB-INF/jsp/dependente/cadastro.jsp" />
            
        </div> <!-- Formul�rio de cadastro (fim) -->
        
        <!-- Tabela de listagem (come�o)
        =======================-->
        <div class="tab-pane" id="lista">
            
            <c:import url="/WEB-INF/jsp/dependente/lista.jsp" />
            
        </div><!-- Tabela de listagem (fim) -->
    </div>
</div>
<c:import url="/WEB-INF/jsp/footer.jsp" />