<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="v" %>

<form action="#" method="post" id="formCadastro">

    <input type="hidden" id="method" name="_method" value="post" />

    <div>
        <label for="id">C�digo:</label>
        <input type="text" class="form-control" id="id" name="id" value="${id}" />
        <v:validationMessage name="id" />
    </div>

    <div>
        <label for="cpfResponsavel">CPF do Respons�vel:</label>
        <input type="text" class="form-control" id="cpfResponsavel" name="cpfResponsavel" value="${cpfResponsavel}" />
        <v:validationMessage name="cpfResponsavel" />
    </div>

    <div>
        <label for="grauParentesco">Grau de parentesco do respons�vel:</label>
        <input type="text" class="form-control" id="grauParentesco" name="dependente.grauParentesco" value="${dependente.grauParentesco}" />
        <v:validationMessage name="dependente.grauParentesco" />
    </div>

    <div>
        <label for="nome">Nome:</label>
        <input type="text" class="form-control" id="nome" name="dependente.nome" 
               data-validation="required" data-validation-error-msg="Campo obrigat�rio" value="${dependente.nome}" />
        <v:validationMessage name="dependente.nome" />
    </div>

    <div>
        <label for="cpf">CPF:</label>
        <input type="text" class="form-control" class="form-control" id="cpf" name="dependente.cpf" value="${dependente.cpf}" />
        <v:validationMessage name="dependente.cpf" />
    </div>

    <div>
        <label for="rg">RG:</label>
        <input type="text" class="form-control" id="rg" name="dependente.rg" value="${dependente.rg}" 
               maxlength="14"/>
    </div>

    <div>
        <label for="passaporte">Passaporte:</label>
        <input type="text" class="form-control" id="passaporte" name="dependente.passaporte" value="${dependente.passaporte}"
               maxlength="50" />
    </div>

    <div>
        <label for="dataNascimento">Data Nascimento:</label>
        <input type="text" class="form-control" id="dataNascimento" name="dependente.dataNascimento" value="${dependente.dataNascimento}"/>
        <v:validationMessage name="dependente.dataNascimento" />
    </div>

    <div>
        <label for="sexo">Sexo:</label>
        <select class="form-control" id="sexo" name="dependente.sexo">
            <option value="M">Masculino</option>
            <option value="F">Feminino</option>
            <option value="O">Outro</option>
        </select>
        <v:validationMessage name="dependente.sexo" />
    </div>

    <!-- Accordion de endere�o
    ======================-->
    <v:validationMessage name="dependente.endereco" />
    <div class="col-md-12">
        <h2>Informa��es adicionais</h2>

        <div class="panel-group" id="accordion">

            <div class="panel panel-default">
                <!-- Cabe�alho (come�o)
                ==================-->
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#endereco">
                            Endere�o a ser cadastrado\atualizado
                        </a>
                    </h4>
                </div> <!-- Cabe�alho (fim) -->

                <!-- Corpo (come�o)
                ==================-->
                <div class="panel-collapse collapse in" id="endereco">
                    <div class="panel-body">

                        <div>
                            <label for="numero">N�mero:</label>
                            <input type="text" class="form-control" id="numero" name="endereco.numero" value="${endereco.numero}"/>
                            <v:validationMessage name="endereco.numero" />
                        </div>

                        <div>
                            <label for="cep">CEP:</label>
                            <input type="text" class="form-control" id="cep" name="endereco.cep" value="${endereco.cep}" />
                        </div>

                        <div>
                            <label for="rua">Rua:</label>
                            <input type="text" class="form-control" id="rua" name="endereco.rua" value="${endereco.rua}"/>
                            <v:validationMessage name="endereco.rua" />
                        </div>

                        <div>
                            <label for="bairro">Bairro</label>
                            <input type="text" class="form-control" id="bairro" name="endereco.bairro" value="${endereco.bairro}"/>
                            <v:validationMessage name="endereco.bairro" />
                        </div>

                        <div>
                            <label for="complemento">Complemento:</label>
                            <input type="text" class="form-control" id="complemento" name="endereco.complemento" value="${endereco.complemento}" />
                        </div>

                        <div>
                            <label for="uf">UF:</label>
                            <select class="form-control" id="uf" name="endereco.estado"/>
                            </select>
                        </div>

                        <div>
                            <label for="cidade">Cidade:</label>
                            <select class="form-control" id="cidade" name="endereco.cidade"/>
                            </select>
                            <v:validationMessage name="endereco.cidade" />
                        </div>

                    </div>
                </div><!-- Corpo (fim) -->

            </div>

        </div>

    </div>

    <div>
        <input type="button" id="btnSalvar" value="Salvar" class="btn btn-primary" />
        <input type="button" id="btnDeletar" value="Deletar" class="btn btn-danger" />
        <input type="reset" id="btnReset" value="Limpar tela" class="btn btn-default" />
    </div>
</form>

<script>
    ROOT = '<c:url value="/" />';
    CONTROLLER = 'dependente';
</script>
<script src="<c:url value='/js/jquery/util/utilitarios.js' />"></script>
<script src="<c:url value='/js/jquery/util/utilitarios-pessoa.js' />"></script>
<script src="<c:url value='/js/jquery/util/eventos.js' />"></script>
<script>
    buscaRegistroPeloCodigo(function (data, campoIdTratado)
    {
        var pessoa = data;
        //Pessoa
        $("#id").val(campoIdTratado);
        $("#nome").val(pessoa.nome);
        $("#cpf").val(pessoa.cpf);
        $("#rg").val(pessoa.rg);
        $("#passaporte").val(pessoa.passaporte);
        $("#dataNascimento").val(getDataFormatada(pessoa.dataNascimento));
        $("#grauParentesco").val(pessoa.grauParentesco);
        setSelectByValueOrText("sexo", getSexo(pessoa.sexo), true);
    });
</script>