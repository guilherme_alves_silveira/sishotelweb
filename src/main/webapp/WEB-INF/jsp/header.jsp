<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sistema de Hotelaria</title>
        <!-- icon -->
        <link rel="icon" href="<c:url value='/images/hotel.png' />" />
        <!-- css -->
        <link rel="stylesheet" href="<c:url value='/css/bootstrap/bootstrap.css' />">
        <link rel="stylesheet" href="<c:url value='/css/site.css' />">
        <link rel="stylesheet" href="<c:url value='/css/jquery/images' />">
        <link rel="stylesheet" href="<c:url value='/css/jquery/jquery-ui.css' />">
        <link rel="stylesheet" href="<c:url value='/css/jquery/jquery-ui.structure.css' />">
        <link rel="stylesheet" href="<c:url value='/css/jquery/jquery-ui.theme.css' />">
        <link rel="stylesheet" href="<c:url value='/css/jtable/jtable.css' />">
        <!-- js -->
        <script src="<c:url value='/js/jquery/jquery.js'/>"></script>
        <script src="<c:url value='/js/jquery/mask.js'/>"></script>
        <script src="<c:url value='/js/jquery/bootstrap.js'/>"></script>
        <script src="<c:url value='/js/jquery/jquery-ui.js'/>"></script>
        <script src="<c:url value='/js/jquery/jquery.jtable.js'/>"></script>
        <script src="<c:url value='/js/jquery/mask-money.js' />"></script>
    </head>
    <body>
        <nav>
            <ul class="nav nav-tabs">
                <li><a href="<c:url value='/' />">Home</a></li>
                <li class="dropdown"> <!-- Utilizado para apresentar o dropdown menu -->
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <!-- Item que aparecer� como cabe�alho -->
                        Cadastros <b class="caret"></b> <!-- caret � uma classe que exibe uma seta para baixo -->
                    </a>
                    <ul class="dropdown-menu"> <!-- Itens internos que ser�o exibidos se clicado no link com o data-toggle dropdown -->
                        <li><a href="<c:url value='/hospede/form' />">Hospedes</a></li>
                        <li><a href="<c:url value='/dependente/form' />">Dependentes</a></li>
                        <li><a href="<c:url value='/funcionario/form' />">Funcion�rios</a></li>
                        <li><a href="<c:url value='/quarto/form' />">Quarto</a></li>  
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        Movimenta��es <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Reservas</a></li>
                        <li><a href="#">Hospedagem</a></li>
                    </ul>
                </li>
                <li>
                    <c:choose>
                        <c:when test="${usuarioLogado.logado}">
                            <a href="<c:url value='/login/desloga' />">Deslogar</a>
                        </c:when>
                        <c:otherwise>
                            <a href="<c:url value='/login/form' />">Logar</a>
                        </c:otherwise>
                    </c:choose>
                </li>
            </ul>
        </nav>
        <div class="container">
            <main class="col-sm-8">