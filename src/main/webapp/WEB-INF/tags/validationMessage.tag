<%@ attribute name="name" required="true" %>
<span class="validation-message">
    ${errors.from(name)}
</span>