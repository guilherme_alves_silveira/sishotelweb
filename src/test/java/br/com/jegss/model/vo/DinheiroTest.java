package br.com.jegss.model.vo;

import java.math.BigDecimal;
import static org.junit.Assert.*;
import org.junit.Test;

public class DinheiroTest
{

    @Test
    public void deveRetornarBigDecimalValidoComValorDe100Virgula50()
    {
        Dinheiro dinheiro = new Dinheiro("R$ ", "100,50");
        assertEquals(new BigDecimal("100.50"), dinheiro.getValor());
    }

    @Test
    public void deveRetornarBigDecimalValidoComValorDe100Ponto50()
    {
        Dinheiro dinheiro = new Dinheiro("R$", "100.50");
        assertEquals(new BigDecimal("100.50"), dinheiro.getValor());
    }

    @Test(expected = IllegalArgumentException.class)
    public void naoDeveAceitarMontanteEmOrdemIncorreta()
    {
        Dinheiro dinheiro = new Dinheiro("100 R$", "100.50");
    }

    @Test
    public void deveAceitarDinheiroComValorQuePossueStringDeDoisPontos()
    {
        Dinheiro dinheiro = new Dinheiro("R$", "6.000,50");
        assertEquals(new BigDecimal("6000.50"), dinheiro.getValor());
    }

    @Test
    public void deveAceitarDinheiroComValorQuePossueStringComVariosPontos()
    {
        Dinheiro dinheiro = new Dinheiro("R$", "6.000.000,50");
        assertEquals(new BigDecimal("6000000.50"), dinheiro.getValor());
    }

    @Test(expected = IllegalArgumentException.class)
    public void deveLancarExcecaoSeDinheiroNaoConterMontanteNemValor()
    {
        Dinheiro dinheiro = new Dinheiro(null, null);
    }

    @Test
    public void deveAceitarValorMesmoSeConterEspacos()
    {
        Dinheiro dinheiro = new Dinheiro("R$ ", "         100,50            ");
        assertEquals(new BigDecimal("100.50"), dinheiro.getValor());
    }

    @Test(expected = IllegalArgumentException.class)
    public void naoDeveAceitarValoresInvalidosDeDinheiro()
    {
        Dinheiro dinheiro = new Dinheiro("R$ ", "100%%,50");
    }
}
